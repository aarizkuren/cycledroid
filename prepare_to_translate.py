#!/usr/bin/python

import sys
import re

def prepare_output(filename):
	desc = open(filename, 'r')
	content = desc.read()
	desc.close()
	content = re.sub('<[^<]+?>', '', content)
	content = re.sub('<!--[ ]*', '', content)
	content = re.sub('[ ]*-->', '', content)
	content = re.sub('help.*html', '', content)
	content = re.sub('@string/.*?\n', '', content)
	content = re.sub('  +', '', content)
	content = re.sub('\t', '', content)
	content = re.sub('\n+', '\n', content)
	content = re.sub('^\n*', '', content)
	content = re.sub('&ndash;', '-', content)
	content = re.sub('&#x2026;', '...', content)
	content = re.sub('&#8230;', '...', content)
	content = re.sub('&#8211;', '-', content)
	content = re.sub('\\\\n', ' ', content)
	content = re.sub('\\\\', '', content)
	lines = content.split('\n')
	result = []
	for line in lines:
		if line not in result:
			result.append(line)
	print '\n'.join(result)

print 'CycleDroid (wesprzyj)'
prepare_output('res/values-pl/strings.xml')
prepare_output('res/values-pl-v11/strings.xml')
prepare_output('assets/help-pl.html')
prepare_output('prod/market/description/google_play-pl.txt')
