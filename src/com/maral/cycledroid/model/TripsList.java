/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Observable;
import java.util.Observer;

public class TripsList extends Observable implements List<Trip>, Observer {
	// we don't remove observers from trips after their removal because it doesn't matter
	
	private List<Trip> list = new ArrayList<Trip>();
	
	public Trip getById(long id) {
		for(Trip trip : list)
			if(trip.getId() == id)
				return trip;
		return null;
	}
	
	public void add(int location, Trip object) {
		setChanged();
		object.addObserver(this);
		list.add(location, object);
	}
	
	public boolean add(Trip object) {
		setChanged();
		object.addObserver(this);
		return list.add(object);
	}
	
	public boolean addAll(Collection<? extends Trip> arg0) {
		setChanged();
		for(Trip trip : arg0)
			trip.addObserver(this);
		return list.addAll(arg0);
	}
	
	public boolean addAll(int arg0, Collection<? extends Trip> arg1) {
		setChanged();
		for(Trip trip : arg1)
			trip.addObserver(this);
		return list.addAll(arg0, arg1);
	}
	
	public void clear() {
		setChanged();
		list.clear();
	}
	
	public boolean contains(Object object) {
		return list.contains(object);
	}
	
	public boolean containsAll(Collection<?> arg0) {
		return list.containsAll(arg0);
	}
	
	public Trip get(int location) {
		return list.get(location);
	}
	
	public int indexOf(Object object) {
		return list.indexOf(object);
	}
	
	public boolean isEmpty() {
		return list.isEmpty();
	}
	
	public Iterator<Trip> iterator() {
		return list.iterator();
	}
	
	public int lastIndexOf(Object object) {
		return list.lastIndexOf(object);
	}
	
	public ListIterator<Trip> listIterator() {
		return list.listIterator();
	}
	
	public ListIterator<Trip> listIterator(int location) {
		return list.listIterator(location);
	}
	
	public Trip remove(int location) {
		setChanged();
		return list.remove(location);
	}
	
	public boolean remove(Object object) {
		setChanged();
		return list.remove(object);
	}
	
	public boolean removeAll(Collection<?> arg0) {
		setChanged();
		return list.removeAll(arg0);
	}
	
	public boolean retainAll(Collection<?> arg0) {
		setChanged();
		return list.retainAll(arg0);
	}
	
	public Trip set(int location, Trip object) {
		setChanged();
		object.addObserver(this);
		return list.set(location, object);
	}
	
	public int size() {
		return list.size();
	}
	
	public List<Trip> subList(int start, int end) {
		return list.subList(start, end);
	}
	
	public Object[] toArray() {
		return list.toArray();
	}
	
	public <T> T[] toArray(T[] array) {
		return list.toArray(array);
	}
	
	public void update(Observable observable, Object data) {
		setChanged();
	}
}
