/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.widget.pager;

import android.content.Context;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.AttributeSet;

public class CyclicViewPager extends ViewPager {
	public CyclicViewPager(Context context) {
		super(context);
	}
	
	public CyclicViewPager(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	public void setAdapter(PagerAdapter adapter) {
		super.setAdapter(adapter);
		setCurrentItem(0, false);
	}
	
	@Override
	public void setCurrentItem(int item) {
		throw new UnsupportedOperationException("Use setCurrentItem(int, boolean) instead.");
	}
	
	@Override
	public void setCurrentItem(int item, boolean smoothScroll) {
		if(getAdapter() instanceof CyclicPagerAdapter) {
			CyclicPagerAdapter adapter = (CyclicPagerAdapter)getAdapter();
			if(item < adapter.getRealCount()) {
				int count = adapter.getRealCount();
				item += count * (adapter.getCount() / 2 / count);
			}
		}
		super.setCurrentItem(item, smoothScroll);
	}
}
