/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.widget;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.CheckBox;
import android.widget.Checkable;
import android.widget.LinearLayout;

// http://tokudu.com/2010/android-checkable-linear-layout/
public class CheckableLinearLayout extends LinearLayout implements Checkable {
	private CheckBox checkBox;
	
	public CheckableLinearLayout(Context context, AttributeSet attrs) {
		super(context, attrs);
	}
	
	@Override
	protected void onFinishInflate() {
		super.onFinishInflate();
		for(int i = 0; i < getChildCount(); ++i) {
			View view = getChildAt(i);
			if(view instanceof CheckBox)
				checkBox = (CheckBox)view;
		}
	}
	
	public boolean isChecked() {
		return checkBox != null? checkBox.isChecked() : false;
	}
	
	public void setChecked(boolean checked) {
		if(checkBox != null)
			checkBox.setChecked(checked);
	}
	
	public void toggle() {
		if(checkBox != null)
			checkBox.toggle();
	}
}
