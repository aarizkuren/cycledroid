/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.exporter;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.Writer;

import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.model.Trip;

public class ExportTripTask extends ExtendedAsyncTask {
	private final String exportPath;
	private final Exporter exporter;
	private final Trip trip;
	private boolean exported = false;
	
	public ExportTripTask(AsyncTaskReceiver receiver, String exportPath, Exporter exporter, Trip trip) {
		super(receiver);
		this.exportPath = exportPath;
		this.exporter = exporter;
		this.trip = trip;
	}
	
	@Override
	protected void executeTask() {
		Writer writer = null;
		try {
			File file = new File(exportPath);
			file.createNewFile();
			writer = new BufferedWriter(new FileWriter(file));
			exported = exporter.exportTrip(trip, writer, file, this);
		} catch(Exception exception) {} finally {
			try {
				if(writer != null)
					writer.close();
			} catch(Exception exception) {}
		}
	}
	
	public boolean wasExported() {
		return exported;
	}
	
	public String getExportPath() {
		return exportPath;
	}
	
	public Trip getTrip() {
		return trip;
	}
}
