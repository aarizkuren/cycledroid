/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.exporter;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.LineNumberReader;
import java.io.Reader;

import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.model.Trip;

public class ImportTripTask extends ExtendedAsyncTask {
	private final String importPath;
	private final Exporter exporter;
	private Trip importedTrip = null;
	
	public ImportTripTask(AsyncTaskReceiver receiver, String importPath, Exporter exporter) {
		super(receiver);
		this.importPath = importPath;
		this.exporter = exporter;
	}
	
	@Override
	protected void executeTask() {
		LineNumberReader lineNumberReader = null;
		Reader reader = null;
		try {
			File file = new File(importPath);
			lineNumberReader = new LineNumberReader(new FileReader(file));
			lineNumberReader.skip(Long.MAX_VALUE);
			int lines = lineNumberReader.getLineNumber();
			lineNumberReader.close();
			lineNumberReader = null;
			reader = new BufferedReader(new FileReader(file));
			importedTrip = exporter.importTrip(reader, file, lines, this);
		} catch(Exception exception) {} finally {
			try {
				if(lineNumberReader != null)
					lineNumberReader.close();
			} catch(Exception excepion) {} finally {
				try {
					if(reader != null)
						reader.close();
				} catch(Exception exception) {}
			}
		}
	}
	
	public String getImportPath() {
		return importPath;
	}
	
	public Trip getImportedTrip() {
		return importedTrip;
	}
}
