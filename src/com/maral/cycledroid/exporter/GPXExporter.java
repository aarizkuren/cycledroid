/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.exporter;

import java.io.File;
import java.io.Reader;
import java.io.Writer;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import java.util.TimeZone;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager.NameNotFoundException;

import com.maral.cycledroid.R;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.MyCursor;
import com.maral.cycledroid.model.Trip;

public class GPXExporter implements Exporter {
	private String name;
	private final Database database;
	
	public GPXExporter(Context context, Database database) {
		try {
			PackageInfo info = context.getPackageManager().getPackageInfo(context.getPackageName(), 0);
			name = context.getString(info.applicationInfo.labelRes) + " " + info.versionName;
		} catch(NameNotFoundException exception) {
			name = context.getString(R.string.app_label);
		}
		this.database = database;
	}
	
	private static String escape(String input) {
		input = input.replace("&", "&amp;");
		input = input.replace(">", "&gt;");
		input = input.replace("<", "&lt;");
		input = input.replace("'", "&apos;");
		input = input.replace("\"", "&quot;");
		input = input.replace("\n", "&#13;");
		return input;
	}
	
	@SuppressLint("SimpleDateFormat")
	public boolean exportTrip(Trip trip, Writer writer, File file, ExportTripTask task) {
		task.updateProgress(0);
		boolean successful = false;
		database.beginTransaction();
		MyCursor pointsCursor = new MyCursor(database.getPointsCursor(trip, 1));
		try {
			// XML format
			SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
			timeFormatter.setTimeZone(TimeZone.getTimeZone("UTC"));
			
			writer.write("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?>\r\n");
			writer.write("\r\n");
			writer.write("<gpx xmlns=\"http://www.topografix.com/GPX/1/1\" version=\"1.1\" creator=\"" + name
					+ "\">\r\n");
			writer.write("\t<trk>\r\n");
			writer.write("\t\t<name>" + escape(trip.getName()) + "</name>\r\n");
			writer.write("\t\t<desc>" + escape(trip.getDescription()) + "</desc>\r\n");
			
			int onePercent = pointsCursor.getCount() / 100;
			if(onePercent == 0) // to avoid dividing by zero in a loop
				onePercent = 1;
			int exported = 0;
			long previousTime = -1;
			for(pointsCursor.moveToFirst(); !pointsCursor.isAfterLast(); pointsCursor.moveToNext()) {
				String latitude = Float.toString(pointsCursor.getFloat(Database.POINT_LATITUDE));
				String longitude = Float.toString(pointsCursor.getFloat(Database.POINT_LONGITUDE));
				String altitude = Float.toString(pointsCursor.getFloat(Database.POINT_ALTITUDE));
				String speed = String.format(Locale.ENGLISH, "%f", pointsCursor.getFloat(Database.POINT_SPEED));
				// English locale is used to have a dot as a decimal separator
				long time = pointsCursor.getLong(Database.POINT_TIME);
				Date date = new Date(time);
				
				if(time - previousTime > Trip.PART_TIME) {
					if(previousTime != -1)
						writer.write("\t\t</trkseg>\r\n");
					writer.write("\t\t<trkseg>\r\n");
				}
				previousTime = time;
				
				writer.write("\t\t\t<trkpt lat=\"" + latitude + "\" lon=\"" + longitude + "\">\r\n");
				writer.write("\t\t\t\t<speed>" + speed + "</speed>\r\n");
				writer.write("\t\t\t\t<ele>" + altitude + "</ele>\r\n");
				writer.write("\t\t\t\t<time>" + timeFormatter.format(date) + "</time>\r\n");
				writer.write("\t\t\t</trkpt>\r\n");
				
				// don't do it often - it really slows:
				if(++exported % onePercent == 0)
					task.updateProgress((int)(exported / onePercent));
			}
			
			if(previousTime != -1)
				writer.write("\t\t</trkseg>\r\n");
			writer.write("\t</trk>\r\n");
			writer.write("</gpx>\r\n");
			successful = true;
		} catch(Exception exception) {
			return false;
		} finally {
			task.updateProgress(100);
			pointsCursor.close();
			database.endTransaction(successful);
		}
		
		return true;
	}
	
	public Trip importTrip(Reader reader, File file, int lines, ImportTripTask task) {
		throw new UnsupportedOperationException("GPX file cannot be imported.");
	}
}
