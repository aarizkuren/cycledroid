/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.database;

import android.database.Cursor;

public class MyCursor {
	private final Cursor cursor;
	
	public MyCursor(Cursor cursor) {
		this.cursor = cursor;
	}
	
	public int getInt(String column) {
		return cursor.getInt(cursor.getColumnIndex(column));
	}
	
	public String getString(String column) {
		return cursor.getString(cursor.getColumnIndex(column));
	}
	
	public float getFloat(String column) {
		return cursor.getFloat(cursor.getColumnIndex(column));
	}
	
	public long getLong(String column) {
		return cursor.getLong(cursor.getColumnIndex(column));
	}
	
	public byte[] getBlob(String column) {
		return cursor.getBlob(cursor.getColumnIndex(column));
	}
	
	public double getDouble(String column) {
		return cursor.getDouble(cursor.getColumnIndex(column));
	}
	
	public boolean isNull(String column) {
		return cursor.isNull(cursor.getColumnIndex(column));
	}
	
	public void close() {
		cursor.close();
	}
	
	public boolean isAfterLast() {
		return cursor.isAfterLast();
	}
	
	public boolean moveToFirst() {
		return cursor.moveToFirst();
	}
	
	public boolean moveToNext() {
		return cursor.moveToNext();
	}
	
	public int getCount() {
		return cursor.getCount();
	}
}
