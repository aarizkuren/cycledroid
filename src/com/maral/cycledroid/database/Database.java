/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.database;

import android.database.Cursor;
import android.location.Location;

import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.TripsList;

public interface Database {
	// tables' and columns' names:
	public static final String ID_COLUMN = "_id";
	
	public static final String TRIP_TABLE = "Trip";
	public static final String TRIP_NAME = "name";
	public static final String TRIP_DESCRIPTION = "description";
	public static final String TRIP_MAX_SPEED = "max_speed";
	public static final String TRIP_DISTANCE = "distance";
	public static final String TRIP_TIME = "time";
	public static final String TRIP_ELEVATION_ASC = "elevation_asc";
	public static final String TRIP_ELEVATION_DESC = "elevation_desc";
	public static final String TRIP_MIN_ALTITUDE = "min_altitude";
	public static final String TRIP_MAX_ALTITUDE = "max_altitude";
	public static final String TRIP_TOTAL_TIME = "total_time";
	public static final String TRIP_INITIAL_ALTITUDE = "initial_altitude";
	public static final String TRIP_FINAL_ALTITUDE = "final_altitude";
	public static final String TRIP_START_TIME = "start_time";
	public static final String TRIP_END_TIME = "end_time";
	
	public static final String POINT_TABLE = "Point";
	public static final String POINT_TRIP = "trip";
	public static final String POINT_LATITUDE = "latitude";
	public static final String POINT_LONGITUDE = "longitude";
	public static final String POINT_ALTITUDE = "altitude";
	public static final String POINT_TIME = "time";
	public static final String POINT_SPEED = "speed";
	
	public TripsList getTripsList();
	
	public void deleteTrip(Trip trip);
	
	public Trip createTripAndAdd(String name, String description);
	
	public Trip createTripNoAdd(String name, String description);
	
	public void addTrip(Trip trip);
	
	public void addPoint(Location point, Trip trip);
	
	public void updateTrip(Trip trip, String name, String description);
	
	public int getPointsCount(Trip trip);
	
	public Cursor getPointsCursor(Trip trip, int interval);
	
	public void increaseTotalTime(Trip trip, float deltaTime);
	
	public void beginTransaction();
	
	public void endTransaction(boolean successful);
	
	public void finish(boolean canClose);
}
