/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.database;

import java.util.Arrays;
import java.util.List;

import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.model.Trip;

public class DeleteTripsTask extends ExtendedAsyncTask {
	private final List<Trip> tripsList;
	private final Database database;
	
	public DeleteTripsTask(AsyncTaskReceiver receiver, List<Trip> tripsList, Database database) {
		super(receiver);
		this.tripsList = tripsList;
		this.database = database;
	}
	
	public DeleteTripsTask(AsyncTaskReceiver receiver, Trip trip, Database database) {
		this(receiver, Arrays.asList(new Trip[] {trip}), database);
	}
	
	@Override
	protected void executeTask() {
		database.beginTransaction(); // preserve consistent state
		boolean successful = false;
		try {
			for(Trip trip : tripsList)
				database.deleteTrip(trip);
			successful = true;
		} finally {
			database.endTransaction(successful);
		}
	}
}
