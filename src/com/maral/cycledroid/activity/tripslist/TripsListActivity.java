/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.tripslist;

import java.io.File;
import java.util.Calendar;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.os.Environment;
import android.preference.PreferenceManager;
import android.view.ContextMenu;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.AdapterView;
import android.widget.AdapterView.AdapterContextMenuInfo;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.BaseAdapter;
import android.widget.Toast;

import com.maral.cycledroid.AppInfo;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.DonateActivity;
import com.maral.cycledroid.activity.FormatterSettings;
import com.maral.cycledroid.activity.HelpActivity;
import com.maral.cycledroid.activity.TextFormatter;
import com.maral.cycledroid.activity.TripEditActivity;
import com.maral.cycledroid.activity.file.ChooseFileActivity;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.activity.trip.TripActivity;
import com.maral.cycledroid.asynctask.AsyncTaskQueue;
import com.maral.cycledroid.asynctask.AsyncTaskQueueImpl;
import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.database.DeleteTripsTask;
import com.maral.cycledroid.database.GetSQLiteInstanceTask;
import com.maral.cycledroid.exporter.CSVExporter;
import com.maral.cycledroid.exporter.Exporter;
import com.maral.cycledroid.exporter.ImportTripTask;
import com.maral.cycledroid.formatter.PairFormatter;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.TripsList;
import com.maral.cycledroid.service.ServiceState.State;
import com.maral.cycledroid.service.TripService;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

public final class TripsListActivity extends ListActivity {
	private class Controller implements OnClickListener, OnItemClickListener, AsyncTaskReceiver, Observer {
		private boolean ourTask(ExtendedAsyncTask task) {
			for(long id : tasksIds)
				if(task.getId() == id)
					return true;
			return false;
		}
		
		public void update(Observable observable, Object object) {
			if(observable == tripsList || observable == formatter)
				updateTripsList();
		}
		
		public void taskFinishes(ExtendedAsyncTask task) {
			if(ourTask(task)) {
				if(task instanceof ImportTripTask) {
					Trip importedTrip = ((ImportTripTask)task).getImportedTrip();
					String path = ((ImportTripTask)task).getImportPath();
					CharSequence message;
					if(importedTrip == null)
						message = TextFormatter.getText(TripsListActivity.this, R.string.toast_trip_not_imported, path);
					else {
						++importSuccess;
						message = TextFormatter.getText(TripsListActivity.this, R.string.toast_trip_imported,
								importedTrip.getName(), path);
					}
					Toast.makeText(TripsListActivity.this, message, Toast.LENGTH_LONG).show();
					asyncTaskQueue.clearLast();
					if(++importFinished == importTotal) {
						removeProgressDialog();
						if(importTotal == importSuccess)
							message = getString(R.string.toast_all_imported, Toast.LENGTH_LONG);
						else
							message = getString(R.string.toast_not_all_imported, importSuccess, importTotal);
						Toast.makeText(TripsListActivity.this, message, Toast.LENGTH_LONG).show();
					} else {
						progressDialog.setSecondaryProgress(100 * (importFinished + 1) / importTotal);
						progressDialog.setProgress(100 * importFinished / importTotal);
					}
				} else if(task instanceof GetSQLiteInstanceTask) {
					removeProgressDialog();
					GetSQLiteInstanceTask getSQLiteInstanceTask = (GetSQLiteInstanceTask)task;
					database = getSQLiteInstanceTask.getResult();
					(new AppInfo(TripsListActivity.this)).setCurrentVersion(getVersionCode());
					afterDatabaseOpen();
					asyncTaskQueue.clearLast();
				} else if(task instanceof DeleteTripsTask) {
					removeProgressDialog();
					asyncTaskQueue.clearLast();
				}
			}
		}
		
		public void taskStarts(ExtendedAsyncTask task) {
			if(ourTask(task)) {
				if(task instanceof ImportTripTask) {
					if(progressDialog == null) {
						progressDialog = new ProgressDialog(TripsListActivity.this);
						progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
						progressDialog.setMax(100);
						progressDialog.setCancelable(false);
						progressDialog.setMessage(getString(R.string.progress_importing));
						progressDialog.show();
					}
					progressDialog.setSecondaryProgress(100 * (importFinished + 1) / importTotal);
				} else if(task instanceof GetSQLiteInstanceTask) {
					progressDialog = new ProgressDialog(TripsListActivity.this);
					progressDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
					progressDialog.setMax(100);
					progressDialog.setCancelable(false);
					progressDialog.setMessage(getString(R.string.progress_updating_database));
					progressDialog.show();
				} else if(task instanceof DeleteTripsTask) {
					progressDialog = new ProgressDialog(TripsListActivity.this);
					progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
					progressDialog.setCancelable(false);
					progressDialog.setMessage(getString(R.string.progress_deleting_trip));
					progressDialog.show();
				}
			}
		}
		
		public void updateProgress(ExtendedAsyncTask task, int progress) {
			if(ourTask(task)) {
				if(task instanceof ImportTripTask) {
					if(progressDialog != null)
						progressDialog.setProgress((100 * importFinished + progress) / importTotal);
				} else if(task instanceof GetSQLiteInstanceTask) {
					if(progressDialog != null)
						progressDialog.setProgress(progress);
				}
			}
		}
		
		public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
			tripActivity(position);
		}
		
		public void onClick(View view) {
			switch(view.getId()) {
				case R.id.add:
					editActivity(true, null);
					break;
				case R.id.no_data_help:
					(new HelpActivity.HelpOpener(TripsListActivity.this, getString(R.string.help_file))).openHelp();
					break;
			}
		}
	}
	
	private static enum DialogType {
		CONFIRM_DELETE, NO_TRIPS,
	}
	
	private static enum RequestType {
		IMPORT_TRIP,
	}
	
	private static enum SavedInstanceKey {
		TRIP_TO_DELETE_ID, IMPORT_TOTAL, IMPORT_SUCCESS, IMPORT_FINISHED, TASKS_IDS,
	}
	
	private final Controller controller = new Controller();
	
	private Settings settings;
	private Database database;
	private AppInfo appInfo;
	private TripsList tripsList;
	private ActivityManager activityManager;
	private PairFormatter formatter;
	
	private static final long NO_ID = -1;
	private long tripToDeleteId = NO_ID;
	
	// total number of trips to import (during one import operation)
	private int importTotal = 0;
	// trips successfully imported
	private int importSuccess = 0;
	// all processed trips - both successfully and unsuccessfully
	private int importFinished = 0;
	private long[] tasksIds = new long[0];
	
	private ProgressDialog progressDialog = null;
	private AsyncTaskQueue asyncTaskQueue;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.trips_list);
		setTitle(R.string.label_trips_list);
		try {
			findViewById(R.id.add).setOnClickListener(controller);
		} catch(NullPointerException exception) {}
		findViewById(R.id.no_data_help).setOnClickListener(controller);
		
		try {
			tripToDeleteId = savedInstanceState.getLong(SavedInstanceKey.TRIP_TO_DELETE_ID.name(), NO_ID);
			importTotal = savedInstanceState.getInt(SavedInstanceKey.IMPORT_TOTAL.name());
			importSuccess = savedInstanceState.getInt(SavedInstanceKey.IMPORT_SUCCESS.name());
			importFinished = savedInstanceState.getInt(SavedInstanceKey.IMPORT_FINISHED.name());
			tasksIds = savedInstanceState.getLongArray(SavedInstanceKey.TASKS_IDS.name());
		} catch(NullPointerException exception) {}
		
		settings = SettingsSystem.getInstance(this);
		asyncTaskQueue = AsyncTaskQueueImpl.getInstance();
		appInfo = new AppInfo(this);
		
		if(asyncTaskQueue.getLast() instanceof GetSQLiteInstanceTask)
			asyncTaskQueue.attach(controller);
		else {
			int lastVersion = appInfo.getCurrentVersion();
			if(lastVersion != getVersionCode()) {
				if(lastVersion <= 31) { // in 32 age was changed to birth year
					SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
					try {
						String ageKey = getString(R.string.key_calories_age);
						String age = preferences.getString(ageKey, null);
						int year = Calendar.getInstance().get(Calendar.YEAR);
						int birthYear = year - Integer.parseInt(age);
						Editor editor = preferences.edit();
						String birthYearKey = getString(R.string.key_calories_birth_year);
						editor.putString(birthYearKey, Integer.toString(birthYear));
						editor.commit();
					} catch(Exception exception) {}
					// may be (but shouldn't) NumberFormatException or NullPointerException
				}
				asyncTaskQueue.attach(controller);
				ExtendedAsyncTask task = new GetSQLiteInstanceTask(asyncTaskQueue, this, settings);
				tasksIds = new long[] {
					task.getId(),
				};
				asyncTaskQueue.addTask(task);
			} else {
				database = DatabaseSQLite.getInstance(this, settings);
				afterDatabaseOpen();
				asyncTaskQueue.attach(controller);
			}
		}
	}
	
	private int getVersionCode() {
		try {
			return getPackageManager().getPackageInfo(getPackageName(), 0).versionCode;
		} catch(NameNotFoundException exception) {
			throw new RuntimeException(exception);
		}
	}
	
	private void afterDatabaseOpen() {
		tripsList = database.getTripsList();
		
		// for screenshots
		/*List<Trip> examples = ExampleTripsList.getTripsList(this);
		if(!tripsList.containsAll(examples))
		tripsList.addAll(examples);*/
		
		formatter = new FormatterSettings(this, settings);
		setListAdapter(new TripsListAdapter(this, tripsList, formatter, false));
		registerForContextMenu(getListView());
		getListView().setOnItemClickListener(controller);
		
		activityManager = new ActivityManagerSettings(this, settings);
		tripsList.addObserver(controller);
		formatter.addObserver(controller);
		
		/* If a user closes application from the "last applications" screen, service still runs, but opening application
		 * causes trips list to appear. The code below tries to deal with such situation and open appropriate
		 * TripActivity in such case. */
		TripService service = TripService.getInstance();
		if(service != null && service.getTrip() != null && TripService.getState().getState() != State.SERVICE_DISABLED) {
			Intent intent = new Intent(this, TripActivity.class);
			// just in case
			intent.putExtra(TripActivity.INTENT_TRIP_ID, service.getTrip().getId());
			startActivity(intent);
		}
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		asyncTaskQueue.detach(controller);
		removeProgressDialog();
		try {
			tripsList.deleteObserver(controller);
			formatter.deleteObserver(controller);
			activityManager.detachActivity(this);
		} catch(NullPointerException exception) {}
		if(database != null)
			database.finish(isFinishing());
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		outState.putLong(SavedInstanceKey.TRIP_TO_DELETE_ID.name(), tripToDeleteId);
		outState.putInt(SavedInstanceKey.IMPORT_SUCCESS.name(), importSuccess);
		outState.putInt(SavedInstanceKey.IMPORT_TOTAL.name(), importTotal);
		outState.putInt(SavedInstanceKey.IMPORT_FINISHED.name(), importFinished);
		outState.putLongArray(SavedInstanceKey.TASKS_IDS.name(), tasksIds);
	}
	
	private void updateTripsList() {
		Runnable run = new Runnable() {
			public void run() {
				((BaseAdapter)getListAdapter()).notifyDataSetChanged();
			}
		};
		runOnUiThread(run);
	}
	
	private Trip getTripByPosition(int position) {
		return tripsList.get(position);
	}
	
	private void editActivity(boolean add, Integer position) {
		Intent intent = new Intent(this, TripEditActivity.class);
		intent.putExtra(TripEditActivity.INTENT_NEW, add);
		if(!add)
			intent.putExtra(TripEditActivity.INTENT_TRIP_ID, (getTripByPosition(position)).getId());
		startActivity(intent);
	}
	
	private void tripActivity(int position) {
		Intent intent = new Intent(this, TripActivity.class);
		intent.putExtra(TripActivity.INTENT_TRIP_ID, (getTripByPosition(position)).getId());
		startActivity(intent);
	}
	
	private void delete(int position) {
		tripToDeleteId = getTripByPosition(position).getId();
		showDialog(DialogType.CONFIRM_DELETE.ordinal());
	}
	
	@Override
	public void onCreateContextMenu(ContextMenu menu, View view, ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, view, menuInfo);
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.context_trips_list, menu);
	}
	
	@Override
	public boolean onContextItemSelected(MenuItem item) {
		int position = ((AdapterContextMenuInfo)item.getMenuInfo()).position;
		switch(item.getItemId()) {
			case R.id.open:
				tripActivity(position);
				return true;
			case R.id.edit:
				editActivity(false, position);
				return true;
			case R.id.delete:
				delete(position);
				return true;
			default:
				return super.onContextItemSelected(item);
		}
	}
	
	private void importData() {
		Intent intent = new Intent(this, ChooseFileActivity.class);
		intent.putExtra(ChooseFileActivity.INTENT_MODE, ChooseFileActivity.MODE_OPEN_MULTIPLE);
		String startPath = appInfo.getLastImportPath();
		if(startPath == AppInfo.NO_IMPORT_PATH || !(new File(startPath).exists()))
			startPath = Environment.getExternalStorageDirectory().getAbsolutePath();
		intent.putExtra(ChooseFileActivity.INTENT_START_PATH, startPath);
		startActivityForResult(intent, RequestType.IMPORT_TRIP.ordinal());
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == Activity.RESULT_OK) {
			RequestType request;
			try {
				request = RequestType.values()[requestCode];
			} catch(IndexOutOfBoundsException exception) {
				return;
			}
			
			switch(request) {
				case IMPORT_TRIP:
					String[] importPaths = data.getStringArrayExtra(ChooseFileActivity.RESULT_PATHS);
					if(importPaths.length == 0)
						return;
					appInfo.setLastImportPath((new File(importPaths[0])).getParent());
					Exporter exporter = new CSVExporter(database);
					importTotal = importPaths.length;
					importFinished = importSuccess = 0;
					tasksIds = new long[importPaths.length];
					for(int i = 0; i < importPaths.length; ++i) {
						ExtendedAsyncTask task = new ImportTripTask(asyncTaskQueue, importPaths[i], exporter);
						tasksIds[i] = task.getId();
						asyncTaskQueue.addTask(task);
					}
			}
		}
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_add, menu);
		inflater.inflate(R.menu.option_import, menu);
		inflater.inflate(R.menu.option_summary, menu);
		inflater.inflate(R.menu.option_export, menu);
		inflater.inflate(R.menu.option_delete, menu);
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		return super.onPrepareOptionsMenu(menu);
	}
	
	private void startMultiActivity(Class<? extends MultiSelectActivity> activity) {
		if(tripsList.size() == 0)
			showDialog(DialogType.NO_TRIPS.ordinal());
		else
			startActivity(new Intent(this, activity));
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.add:
				editActivity(true, null);
				return true;
			case R.id.import_data:
				importData();
				return true;
			case R.id.summary:
				startMultiActivity(SummarySelectActivity.class);
				return true;
			case R.id.export:
				startMultiActivity(MultiExportActivity.class);
				return true;
			case R.id.delete:
				startMultiActivity(MultiDeleteActivity.class);
				return true;
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case CONFIRM_DELETE:
				final Trip tripToDelete = tripsList.getById(tripToDeleteId);
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						if(which == Dialog.BUTTON_POSITIVE) {
							ExtendedAsyncTask task = new DeleteTripsTask(asyncTaskQueue, tripToDelete, database);
							tasksIds = new long[] {
								task.getId(),
							};
							asyncTaskQueue.addTask(task);
							tripToDeleteId = NO_ID;
						}
						removeDialog(id);
					}
				};
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(tripToDelete.getName());
				builder.setMessage(R.string.confirm_message_delete_trip);
				builder.setPositiveButton(R.string.yes, listener);
				builder.setNegativeButton(R.string.no, listener);
				return builder.create();
			case NO_TRIPS:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.error);
				builder.setMessage(R.string.alert_message_no_trips);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	private void removeProgressDialog() {
		if(progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
