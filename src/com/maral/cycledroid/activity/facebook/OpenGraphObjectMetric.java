/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.facebook;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

class OpenGraphObjectMetric extends CustomOpenGraphObject {
	public static final String PROPERTY_LATITUDE = "location:latitude";
	public static final String PROPERTY_LONGITUDE = "location:longitude";
	public static final String PROPERTY_ALTITUDE = "location:altitude";
	public static final String PROPERTY_TIMESTAMP = "timestamp";
	
	private static final DateFormat ISO8601 = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ", Locale.ENGLISH);
	
	public OpenGraphObjectMetric() {
		super(null);
	}
	
	public void setLatitude(float latitude) {
		setProperty(PROPERTY_LATITUDE, Float.toString(latitude));
	}
	
	public void setLongitude(float longitude) {
		setProperty(PROPERTY_LONGITUDE, Float.toString(longitude));
	}
	
	public void setAltitude(float altitude) {
		setProperty(PROPERTY_ALTITUDE, Float.toString(altitude));
	}
	
	public void setTimestamp(long posixTime) {
		setProperty(PROPERTY_TIMESTAMP, ISO8601.format(new Date(posixTime)));
	}
}
