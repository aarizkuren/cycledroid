/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.facebook;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import com.facebook.UiLifecycleHelper;
import com.facebook.widget.FacebookDialog;
import com.facebook.widget.FacebookDialog.Callback;
import com.facebook.widget.FacebookDialog.OpenGraphActionDialogBuilder;
import com.facebook.widget.FacebookDialog.PendingCall;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.HealthCalculatorSettings;
import com.maral.cycledroid.activity.TextFormatter;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.asynctask.AsyncTaskQueue;
import com.maral.cycledroid.asynctask.AsyncTaskQueueImpl;
import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.model.HealthCalculator;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.Unit;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Duration;
import com.maral.cycledroid.model.Unit.Energy;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

public class FacebookActivity extends Activity {
	private class Controller implements Callback, AsyncTaskReceiver {
		public void onError(PendingCall pendingCall, Exception error, Bundle data) {
			lastError = error.getLocalizedMessage();
			showDialog(DialogType.FACEBOOK_ERROR.ordinal());
		}
		
		public void onComplete(PendingCall pendingCall, Bundle data) {
			/*
			int message;
			if(FacebookDialog.getNativeDialogPostId(data) == null)
				message = R.string.toast_facebook_not_shared;
			else
				message = R.string.toast_facebook_shared;
			Toast.makeText(FacebookActivity.this, message, Toast.LENGTH_LONG).show();
			*/
			// this doesn't work...
			finish();
		}
		
		public void updateProgress(ExtendedAsyncTask task, int progress) {}
		
		public void taskStarts(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				progressDialog = new ProgressDialog(FacebookActivity.this);
				progressDialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
				progressDialog.setCancelable(false);
				progressDialog.setMessage(getString(R.string.progress_loading_data));
				progressDialog.show();
			}
		}
		
		public void taskFinishes(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				taskId = ExtendedAsyncTask.NO_ID;
				removeProgressDialog();
				course.setMetrics(((GetFacebookMetricsTask)task).getMetrics());
				finishShowingDialog();
				asyncTaskQueue.clearLast();
			}
		}
	}
	
	private static enum IntentKey {
		TRIP_ID,
	}
	
	private static enum DialogType {
		FACEBOOK_ERROR,
	}
	
	private static enum SavedInstanceKey {
		DIALOG_SHOWN, TASK_ID, LAST_ERROR,
	}
	
	// id of a trip to share
	public static final String INTENT_TRIP_ID = IntentKey.TRIP_ID.name();
	
	private final Controller controller = new Controller();
	
	private long taskId = ExtendedAsyncTask.NO_ID;;
	private boolean dialogShown = false;
	private UiLifecycleHelper uiHelper;
	private ProgressDialog progressDialog;
	private Database database;
	private ActivityManager activityManager;
	private AsyncTaskQueue asyncTaskQueue;
	
	private OpenGraphObjectCourse course;
	private OpenGraphActionBikes bikes;
	
	private String lastError;
	
	public static boolean canShare(Context context) {
		return FacebookDialog.canPresentOpenGraphActionDialog(context,
				FacebookDialog.OpenGraphActionDialogFeature.OG_ACTION_DIALOG);
	}
	
	private void finishShowingDialog() {
		bikes.setCourse(course);
		
		FacebookDialog shareDialog = new OpenGraphActionDialogBuilder(FacebookActivity.this, bikes,
				OpenGraphActionBikes.PROPERTY_COURSE).build();
		uiHelper.trackPendingDialogCall(shareDialog.present());
		dialogShown = true;
	}
	
	private Distance getUsedDistance(Distance realUnit) {
		switch(realUnit) {
			case FT:
			case MI:
			case NMI:
				return Distance.MI;
			case KM:
			case M:
				return Distance.KM;
		}
		throw new IllegalArgumentException("Distance unit " + realUnit + " is not supported.");
	}
	
	private Speed getUsedSpeed(Speed realUnit) {
		switch(realUnit) {
			case FTS:
			case KN:
			case MPH:
				return Speed.FTS;
			case KMH:
			case MS:
				return Speed.MS;
		}
		throw new IllegalArgumentException("Speed unit " + realUnit + " is not supported.");
	}
	
	private Pace getUsedPace(Pace realUnit) {
		switch(realUnit) {
			case MS_PER_FT:
			case S_PER_MI:
			case S_PER_NMI:
				return Pace.MS_PER_FT;
			case MS_PER_M:
			case S_PER_KM:
				return Pace.MS_PER_M;
		}
		throw new IllegalArgumentException("Pace unit " + realUnit + " is not supported.");
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		uiHelper = new UiLifecycleHelper(this, null);
		uiHelper.onCreate(savedInstanceState);
		try {
			dialogShown = savedInstanceState.getBoolean(SavedInstanceKey.DIALOG_SHOWN.name(), false);
			taskId = savedInstanceState.getLong(SavedInstanceKey.TASK_ID.name(), ExtendedAsyncTask.NO_ID);
			lastError = savedInstanceState.getString(SavedInstanceKey.LAST_ERROR.name());
		} catch(NullPointerException exception) {}
		
		Settings settings = SettingsSystem.getInstance(this);
		activityManager = new ActivityManagerSettings(this, settings);
		database = DatabaseSQLite.getInstance(this, settings);
		asyncTaskQueue = AsyncTaskQueueImpl.getInstance();
		asyncTaskQueue.attach(controller);
		
		if(dialogShown)
			return;
		
		long tripId = getIntent().getLongExtra(INTENT_TRIP_ID, -1);
		Trip trip = database.getTripsList().getById(tripId);
		HealthCalculator healthCalculator = new HealthCalculatorSettings(trip, settings);
		
		course = new OpenGraphObjectCourse();
		course.setTitle(trip.getName());
		course.setDescription(trip.getDescription());
		Distance distanceUnit = getUsedDistance(settings.getDistanceUnit());
		course.setDistance(Unit.convertDistance(trip.getDistance(), Trip.UNIT_DISTANCE, distanceUnit), distanceUnit);
		if(settings.getFacebookDuration())
			course.setDurationInMs(Unit.convertDuration(trip.getTime(), Trip.UNIT_DURATION, Duration.MS));
		if(settings.getFacebookSpeed() && trip.getAverageSpeed() != null) {
			Speed speedUnit = getUsedSpeed(settings.getSpeedUnit());
			course.setSpeed(Unit.convertSpeed(trip.getAverageSpeed(), Trip.UNIT_SPEED, speedUnit), speedUnit);
		}
		if(settings.getFacebookCalories() && healthCalculator.getCaloriesBurned() != null)
			course.setCalories(Unit.convertEnergy(healthCalculator.getCaloriesBurned(), HealthCalculator.UNIT_ENERGY,
					Energy.KCAL));
		if(settings.getFacebookPace() && trip.getPaceNetto() != null) {
			Pace paceUnit = getUsedPace(settings.getPaceUnit());
			course.setPace(Unit.convertPace(trip.getPaceNetto(), Trip.UNIT_PACE, paceUnit), paceUnit);
		}
		
		bikes = new OpenGraphActionBikes();
		bikes.setExplicitlyShared(true);
		if(settings.getFacebookTripDate()) {
			if(trip.getStartTime() != null)
				bikes.setStartTime(trip.getStartTime());
			if(trip.getEndTime() != null)
				bikes.setEndTime(trip.getEndTime());
		}
		
		if(!(asyncTaskQueue.getLast() instanceof GetFacebookMetricsTask)) {
			if(settings.getFacebookMap()) {
				ExtendedAsyncTask task = new GetFacebookMetricsTask(asyncTaskQueue, database, trip);
				taskId = task.getId();
				asyncTaskQueue.addTask(task);
			} else
				finishShowingDialog();
		}
	}
	
	@Override
	protected void onResume() {
		super.onResume();
		uiHelper.onResume();
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		uiHelper.onSaveInstanceState(outState);
		outState.putBoolean(SavedInstanceKey.DIALOG_SHOWN.name(), dialogShown);
		outState.putLong(SavedInstanceKey.TASK_ID.name(), taskId);
		outState.putString(SavedInstanceKey.LAST_ERROR.name(), lastError);
	}
	
	@Override
	public void onPause() {
		super.onPause();
		uiHelper.onPause();
	}
	
	@Override
	public void onDestroy() {
		super.onDestroy();
		uiHelper.onDestroy();
		activityManager.detachActivity(this);
		removeProgressDialog();
		database.finish(isFinishing());
		asyncTaskQueue.detach(controller);
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		super.onActivityResult(requestCode, resultCode, data);
		if(data != null) // workaround for the Facebook bug (https://developers.facebook.com/x/bugs/565802870184322/)
			uiHelper.onActivityResult(requestCode, resultCode, data, controller);
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener;
		
		switch(dialogType) {
			case FACEBOOK_ERROR:
				listener = new DialogInterface.OnClickListener() {
					@Override
					public void onClick(DialogInterface dialog, int which) {
						finish();
						removeDialog(id);
					}
				};
				builder.setTitle(R.string.error);
				builder.setMessage(TextFormatter.getText(this, R.string.alert_facebook_error_message, lastError));
				builder.setPositiveButton(R.string.ok, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	private void removeProgressDialog() {
		if(progressDialog != null) {
			progressDialog.dismiss();
			progressDialog = null;
		}
	}
}
