/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.webkit.WebView;
import android.webkit.WebViewClient;

public final class HelpActivity extends Activity {
	private final class Controller implements OnClickListener {
		public void onClick(View view) {
			switch(view.getId()) {
				case R.id.close:
					finish();
			}
		}
	}
	
	private Controller controller = new Controller();
	
	private static enum IntentKey {
		HELP_FILE,
	}
	
	// filename (HTML) to show
	public static final String INTENT_HELP_FILE = IntentKey.HELP_FILE.name();
	
	private Settings settings;
	private ActivityManager activityManager;
	
	public static class HelpOpener {
		private final Context context;
		private final Intent intent;
		
		public HelpOpener(Context context, String helpFile) {
			this.context = context;
			intent = new Intent(context, HelpActivity.class);
			intent.putExtra(INTENT_HELP_FILE, helpFile);
		}
		
		public void openHelp() {
			context.startActivity(intent);
		}
	}
	
	private class HelpActivityClient extends WebViewClient {
		@Override
		public void onPageFinished(WebView view, String url) {
			setTitle(view.getTitle());
		}
	}
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		settings = SettingsSystem.getInstance(this);
		activityManager = new ActivityManagerSettings(this, settings);
		
		setContentView(R.layout.help_activity);
		findViewById(R.id.close).setOnClickListener(controller);
		String helpFile = getIntent().getStringExtra(INTENT_HELP_FILE);
		WebView webView = (WebView)findViewById(R.id.help_view);
		// bug, doesn't work in XML:
		webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
		webView.setWebViewClient(new HelpActivityClient());
		webView.loadUrl("file:///android_asset/" + helpFile);
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityManager.detachActivity(this);
	}
}
