/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.graph;

import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;

class GenerateDataTask extends ExtendedAsyncTask {
	private final GraphDataGenerator generator;
	
	public GenerateDataTask(AsyncTaskReceiver receiver, GraphDataGenerator generator) {
		super(receiver);
		this.generator = generator;
	}
	
	@Override
	protected void onPreExecute() {
		generator.prepareToGenerate();
		super.onPreExecute();
	}
	
	@Override
	protected void executeTask() {
		generator.generateData(this);
	}
	
	public GraphDataGenerator getGenerator() {
		return generator;
	}
}
