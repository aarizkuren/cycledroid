/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.graph;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Observable;
import java.util.Observer;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.Color;
import android.os.AsyncTask.Status;
import android.os.Bundle;
import android.os.Environment;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.jjoe64.graphview.GraphView;
import com.jjoe64.graphview.GraphViewData;
import com.jjoe64.graphview.LabelFormatter;
import com.maral.cycledroid.AppInfo;
import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.DonateActivity;
import com.maral.cycledroid.activity.HelpActivity;
import com.maral.cycledroid.activity.TextFormatter;
import com.maral.cycledroid.activity.file.ChooseFileActivity;
import com.maral.cycledroid.activity.settings.Settings;
import com.maral.cycledroid.activity.settings.Settings.SettingType;
import com.maral.cycledroid.activity.settings.SettingsActivity;
import com.maral.cycledroid.activity.settings.SettingsSystem;
import com.maral.cycledroid.asynctask.AsyncTaskQueue;
import com.maral.cycledroid.asynctask.AsyncTaskQueueImpl;
import com.maral.cycledroid.asynctask.AsyncTaskReceiver;
import com.maral.cycledroid.asynctask.ExtendedAsyncTask;
import com.maral.cycledroid.database.Database;
import com.maral.cycledroid.database.DatabaseSQLite;
import com.maral.cycledroid.formatter.ValueFormatter;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.Unit.Duration;
import com.maral.cycledroid.ui.ActivityManager;
import com.maral.cycledroid.ui.ActivityManagerSettings;

public final class GraphActivity extends Activity {
	private class Controller implements Observer, AsyncTaskReceiver {
		public void taskFinishes(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				List<GraphViewData> data = ((GenerateDataTask)task).getGenerator().getData();
				if(data.size() < MIN_GRAPH_DATA) {
					graphView.setVisibility(View.GONE);
					noDataView.setVisibility(View.VISIBLE);
				} else {
					graphView.setVisibility(View.VISIBLE);
					graphView.setData(data);
					try {
						float viewportStart = savedInstanceState.getFloat(SavedInstanceKey.VIEWPORT_START.name(),
								Float.NaN);
						float viewportSize = savedInstanceState.getFloat(SavedInstanceKey.VIEWPORT_SIZE.name(),
								Float.NaN);
						if(!Float.isNaN(viewportStart) && !Float.isNaN(viewportSize))
							graphView.setViewport(viewportStart, viewportSize);
					} catch(NullPointerException exception) {}
					graphView.setScalable(true);
				}
				refreshing = false;
				reflectInvalidateOptionsMenu();
			}
		}
		
		public void taskStarts(ExtendedAsyncTask task) {
			if(task.getId() == taskId) {
				graphView.setData(((GenerateDataTask)task).getGenerator().getData());
				graphView.refresh();
				graphView.setVisibility(View.VISIBLE);
			}
		}
		
		public void updateProgress(ExtendedAsyncTask task, int progress) {
			if(task.getId() == taskId) {
				int futureCount = ((GenerateDataTask)task).getGenerator().getFutureCount();
				if(futureCount > MIN_GRAPH_DATA) {
					graphView.setFutureCount(futureCount);
					graphView.refresh();
				}
			}
		}
		
		public void update(final Observable observable, final Object data) {
			Runnable run = new Runnable() {
				public void run() {
					if(observable != settings)
						return;
					switch((SettingType)data) {
						case ALTITUDE_UNIT:
							if(graphType == GraphType.DISTANCE_ALTITUDE) {
								yFormatter = new AltitudeFormatter(format, settings.getAltitudeUnit());
								graphView.setFormatters(xFormatter, yFormatter);
								graphView.refresh();
							}
							break;
						case DISTANCE_UNIT:
							if(graphType == GraphType.DISTANCE_ALTITUDE || graphType == GraphType.DISTANCE_SPEED) {
								xFormatter = new DistanceFormatter(format, settings.getDistanceUnit());
								graphView.setFormatters(xFormatter, yFormatter);
								graphView.refresh();
							}
							break;
						case SPEED_UNIT:
							if(graphType == GraphType.DISTANCE_SPEED || graphType == GraphType.TIME_SPEED) {
								yFormatter = new SpeedFormatter(format, settings.getSpeedUnit());
								graphView.setFormatters(xFormatter, yFormatter);
								graphView.refresh();
							}
							break;
						case GRAPH_ALTITUDE_ZERO:
							if(graphType == GraphType.DISTANCE_ALTITUDE) {
								setBounds();
								graphView.refresh();
							}
						default: // uninteresting setting, do nothing
					}
				}
			};
			runOnUiThread(run);
		}
	}
	
	private final Controller controller = new Controller();
	
	private static enum DialogType {
		NO_GRAPH,
	}
	
	private static enum RequestType {
		SAVE_GRAPH,
	}
	
	private static enum SavedInstanceKey {
		VIEWPORT_START, VIEWPORT_SIZE, TASK_ID,
	}
	
	private static enum PreviousInstanceKey {
		BITMAP,
	}
	
	private static enum IntentKey {
		TRIP_ID, GRAPH_TYPE,
	}
	
	// id of a trip to use
	public static final String INTENT_TRIP_ID = IntentKey.TRIP_ID.name();
	
	// type of graph, see below
	public static final String INTENT_TYPE = IntentKey.GRAPH_TYPE.name();
	
	private static enum GraphType {
		DISTANCE_ALTITUDE, DISTANCE_SPEED, TIME_SPEED,
	}
	
	public static final int TYPE_DISTANCE_ALTITUDE = GraphType.DISTANCE_ALTITUDE.ordinal();
	public static final int TYPE_DISTANCE_SPEED = GraphType.DISTANCE_SPEED.ordinal();
	public static final int TYPE_TIME_SPEED = GraphType.TIME_SPEED.ordinal();
	
	private static final int NO_KEY = -1;
	
	private static final int MIN_GRAPH_DATA = 3;
	
	private Bundle savedInstanceState;
	
	private Settings settings;
	private Database database;
	private ValueFormatter format;
	private GraphDataGenerator generator;
	private AppInfo appInfo;
	private AsyncTaskQueue asyncTaskQueue;
	private ActivityManager activityManager;
	
	private Trip trip;
	
	private GraphType graphType;
	private LabelFormatter xFormatter;
	private LabelFormatter yFormatter;
	private GraphView graphView;
	private TextView noDataView;
	// whether refreshing or drawing for the first time
	private boolean refreshing = true;
	private long taskId = ExtendedAsyncTask.NO_ID;
	private Bitmap bitmap = null; // for saving graph as image
	
	@SuppressWarnings("unchecked")
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.graph);
		this.savedInstanceState = savedInstanceState;
		try {
			taskId = savedInstanceState.getLong(SavedInstanceKey.TASK_ID.name(), ExtendedAsyncTask.NO_ID);
		} catch(NullPointerException exception) {}
		graphType = GraphType.values()[getIntent().getIntExtra(INTENT_TYPE, NO_KEY)];
		settings = SettingsSystem.getInstance(this);
		settings.addObserver(controller);
		database = DatabaseSQLite.getInstance(this, settings);
		format = new ValueFormatter();
		appInfo = new AppInfo(this);
		activityManager = new ActivityManagerSettings(this, settings);
		
		long tripId = getIntent().getLongExtra(INTENT_TRIP_ID, NO_KEY);
		trip = database.getTripsList().getById(tripId);
		
		switch(graphType) {
			case DISTANCE_ALTITUDE:
				generator = new AltitudeDistanceGenerator(database, trip);
				break;
			case DISTANCE_SPEED:
				generator = new SpeedDistanceGenerator(database, trip);
				break;
			case TIME_SPEED:
				generator = new SpeedTimeGenerator(database, trip);
				break;
		}
		
		noDataView = (TextView)findViewById(R.id.no_data);
		graphView = (GraphView)findViewById(R.id.graph);
		setBounds();
		
		int label = 0;
		switch(graphType) {
			case DISTANCE_ALTITUDE:
				label = R.string.label_graph_distance_altitude;
				xFormatter = new DistanceFormatter(format, settings.getDistanceUnit());
				yFormatter = new AltitudeFormatter(format, settings.getAltitudeUnit());
				graphView.setColor(getResources().getColor(R.color.distance_altitude));
				break;
			case DISTANCE_SPEED:
				label = R.string.label_graph_distance_speed;
				xFormatter = new DistanceFormatter(format, settings.getDistanceUnit());
				yFormatter = new SpeedFormatter(format, settings.getSpeedUnit());
				graphView.setColor(getResources().getColor(R.color.distance_speed));
				break;
			case TIME_SPEED:
				label = R.string.label_graph_time_speed;
				xFormatter = new TimeFormatter(format, Duration.S);
				yFormatter = new SpeedFormatter(format, settings.getSpeedUnit());
				graphView.setFormatters(xFormatter, yFormatter);
				graphView.setColor(getResources().getColor(R.color.time_speed));
				break;
		}
		graphView.setFormatters(xFormatter, yFormatter);
		setTitle(getString(label, trip.getName()));
		
		Map<PreviousInstanceKey, Object> previousInstance = (Map<PreviousInstanceKey, Object>)getLastNonConfigurationInstance();
		if(previousInstance != null)
			bitmap = (Bitmap)previousInstance.get(PreviousInstanceKey.BITMAP);
		
		asyncTaskQueue = AsyncTaskQueueImpl.getInstance();
		asyncTaskQueue.attach(controller);
		if(!(asyncTaskQueue.getLast() instanceof GenerateDataTask))
			startGenerateDataTask();
	}
	
	@Override
	protected void onDestroy() {
		super.onDestroy();
		activityManager.detachActivity(this);
		settings.deleteObserver(controller);
		if(isFinishing()) {
			try { // error reports show that last task is sometimes null
				if(asyncTaskQueue.getLast().getStatus() == Status.RUNNING)
					asyncTaskQueue.getLast().cancel(true);
			} catch(NullPointerException exception) {}
			asyncTaskQueue.clearLast();
		}
		asyncTaskQueue.detach(controller);
		database.finish(isFinishing());
	}
	
	@Override
	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
		if(graphView != null) {
			outState.putFloat(SavedInstanceKey.VIEWPORT_START.name(), graphView.getViewportStart());
			outState.putFloat(SavedInstanceKey.VIEWPORT_SIZE.name(), graphView.getViewportSize());
		}
		outState.putLong(SavedInstanceKey.TASK_ID.name(), taskId);
	}
	
	@Override
	protected Dialog onCreateDialog(final int id) {
		DialogType dialogType;
		try {
			dialogType = DialogType.values()[id];
		} catch(IndexOutOfBoundsException exception) {
			return super.onCreateDialog(id);
		}
		
		AlertDialog.Builder builder = new AlertDialog.Builder(this);
		DialogInterface.OnClickListener listener = new DialogInterface.OnClickListener() {
			@Override
			public void onClick(DialogInterface dialog, int which) {
				removeDialog(id);
			}
		};
		
		switch(dialogType) {
			case NO_GRAPH:
				builder.setIcon(android.R.drawable.ic_dialog_alert);
				builder.setTitle(R.string.alert_title_no_graph);
				builder.setMessage(R.string.alert_message_no_graph);
				builder.setNeutralButton(R.string.ok, listener);
				return builder.create();
		}
		
		return super.onCreateDialog(id);
	}
	
	private void setBounds() {
		if(graphType == GraphType.DISTANCE_ALTITUDE && settings.getGraphAltitudeZero())
			graphView.setManualMinYAxisBound(0.0f);
		else
			graphView.setManualMinYAxisBound(generator.getMinY());
		graphView.setManualMaxYAxisBound(generator.getMaxY());
		graphView.setViewport(generator.getMinX(), generator.getMaxX() - generator.getMinX());
	}
	
	private void startGenerateDataTask() {
		savedInstanceState = null;
		graphView.setScalable(false);
		ExtendedAsyncTask task = new GenerateDataTask(asyncTaskQueue, generator);
		taskId = task.getId();
		asyncTaskQueue.addTask(task);
	}
	
	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater = getMenuInflater();
		inflater.inflate(R.menu.option_save_graph, menu);
		inflater.inflate(R.menu.option_refresh, menu);
		inflater.inflate(R.menu.option_settings, menu);
		inflater.inflate(R.menu.option_help, menu);
		inflater.inflate(R.menu.option_donate, menu);
		return super.onCreateOptionsMenu(menu);
	}
	
	@Override
	public boolean onPrepareOptionsMenu(Menu menu) {
		menu.findItem(R.id.donate).setVisible(!settings.getRemoveDonate());
		menu.findItem(R.id.save_graph).setEnabled(!refreshing);
		menu.findItem(R.id.refresh).setEnabled(!refreshing);
		return super.onPrepareOptionsMenu(menu);
	}
	
	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		switch(item.getItemId()) {
			case R.id.save_graph:
				saveGraph();
				return true;
			case R.id.refresh:
				refreshing = true;
				reflectInvalidateOptionsMenu();
				graphView.setVisibility(View.GONE);
				noDataView.setVisibility(View.GONE);
				setBounds();
				startGenerateDataTask();
				return true;
			case R.id.settings:
				startActivity(new Intent(this, SettingsActivity.class));
				return true;
			case R.id.help:
				(new HelpActivity.HelpOpener(this, getString(R.string.help_file))).openHelp();
				return true;
			case R.id.donate:
				startActivity(new Intent(this, DonateActivity.class));
				return true;
			default:
				return super.onOptionsItemSelected(item);
		}
	}
	
	private void reflectInvalidateOptionsMenu() {
		try {
			Class<?> classActivity = Class.forName("android.app.Activity");
			Method invalidateOptionsMenu = classActivity.getMethod("invalidateOptionsMenu");
			invalidateOptionsMenu.invoke(this);
		} catch(Exception exception) {}
	}
	
	private void saveGraph() {
		if(graphView.getVisibility() == View.GONE) // graph is not shown <=> no data
			showDialog(DialogType.NO_GRAPH.ordinal());
		else {
			graphView.setDrawingCacheEnabled(true);
			bitmap = Bitmap.createBitmap(graphView.getDrawingCache());
			// Don't do above in onActivityResult() (problem when orientation changes).
			Intent intent = (new Intent(this, ChooseFileActivity.class));
			intent.putExtra(ChooseFileActivity.INTENT_MODE, ChooseFileActivity.MODE_SAVE_SINGLE);
			String startPath = appInfo.getLastSaveGraphPath();
			if(startPath == AppInfo.NO_SAVE_GRAPH_PATH || !(new File(startPath).exists()))
				startPath = Environment.getExternalStorageDirectory().getAbsolutePath();
			intent.putExtra(ChooseFileActivity.INTENT_START_PATH, startPath);
			intent.putExtra(ChooseFileActivity.INTENT_FILENAME_PREFIX, trip.getName());
			intent.putExtra(ChooseFileActivity.INTENT_FILENAME_SUFFIX, ".png");
			startActivityForResult(intent, RequestType.SAVE_GRAPH.ordinal());
		}
	}
	
	@Override
	public Object onRetainNonConfigurationInstance() {
		Map<PreviousInstanceKey, Object> result = new HashMap<PreviousInstanceKey, Object>();
		result.put(PreviousInstanceKey.BITMAP, bitmap);
		return result;
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
		if(resultCode == Activity.RESULT_OK) {
			RequestType requestType;
			try {
				requestType = RequestType.values()[requestCode];
			} catch(IndexOutOfBoundsException exception) {
				return;
			}
			
			switch(requestType) {
				case SAVE_GRAPH:
					String savePath = data.getStringExtra(ChooseFileActivity.RESULT_PATH);
					appInfo.setLastSaveGraphPath((new File(savePath)).getParent());
					for(int x = 0; x < bitmap.getWidth(); ++x)
						// remove transparency
						for(int y = 0; y < bitmap.getHeight(); ++y)
							if(bitmap.getPixel(x, y) == Color.TRANSPARENT)
								bitmap.setPixel(x, y, Color.BLACK);
					try {
						bitmap.compress(CompressFormat.PNG, 100, new FileOutputStream(savePath));
						CharSequence toastText = TextFormatter.getText(this, R.string.toast_saved_file, savePath);
						Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
					} catch(Exception exception) {
						CharSequence toastText = TextFormatter
								.getText(this, R.string.toast_save_graph_failed, savePath);
						Toast.makeText(this, toastText, Toast.LENGTH_LONG).show();
					}
					break;
			}
		}
	}
}
