/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.graph;

import com.jjoe64.graphview.LabelFormatter;
import com.maral.cycledroid.formatter.ValueFormatter;
import com.maral.cycledroid.model.Trip;
import com.maral.cycledroid.model.Unit;
import com.maral.cycledroid.model.Unit.Altitude;

class AltitudeFormatter implements LabelFormatter {
	private final ValueFormatter format;
	private final Altitude altitudeUnit;
	
	public AltitudeFormatter(ValueFormatter format, Altitude altitudeUnit) {
		this.format = format;
		this.altitudeUnit = altitudeUnit;
	}
	
	public String formatLabel(float value) {
		return format.formatAltitude(Unit.convertAltitude(value, Trip.UNIT_ALTITUDE, altitudeUnit), altitudeUnit);
	}
}
