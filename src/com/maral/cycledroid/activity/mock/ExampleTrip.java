/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.mock;

import java.text.ParseException;
import java.text.SimpleDateFormat;

import android.annotation.SuppressLint;
import android.content.Context;
import android.location.Location;

import com.maral.cycledroid.model.Trip;

public abstract class ExampleTrip extends Trip {
	private final Context context;
	private final int name;
	private final int description;
	
	public ExampleTrip(Context context, int name, int description) {
		this.context = context;
		this.name = name;
		this.description = description;
	}
	
	@SuppressLint("SimpleDateFormat")
	protected Long dateFromString(String date) {
		try {
			return (new SimpleDateFormat("d.M.y H:m:s")).parse(date).getTime();
		} catch(ParseException exception) {
			throw new IllegalArgumentException(exception);
		}
	}
	
	@Override
	public String getName() {
		return context.getString(name);
	}
	
	@Override
	public String getDescription() {
		return context.getString(description);
	}
	
	@Override
	public boolean providesEdit() {
		return true;
	}
	
	@Override
	public void edit(String name, String description) {}
	
	@Override
	public boolean providesTracking() {
		return true;
	}
	
	@Override
	public void addPoint(Location point) {}
	
	@Override
	public void increaseTotalTime(float deltaTime) {}
	
	@Override
	public void pause() {}
	
	@Override
	public boolean isPaused() {
		return false;
	}
	
	@Override
	public void stop() {}
	
	@Override
	public Float getFinalAltitude() {
		return getAltitude();
	}
	
	@Override
	public boolean providesGraphs() {
		return true;
	}
	
	@Override
	public boolean providesMap() {
		return true;
	}
	
	@Override
	public boolean providesExport() {
		return true;
	}
	
	@Override
	public boolean providesShare() {
		return true;
	}
}
