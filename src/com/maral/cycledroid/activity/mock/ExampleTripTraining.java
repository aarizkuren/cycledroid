/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.mock;

import android.content.Context;

import com.maral.cycledroid.R;

public class ExampleTripTraining extends ExampleTrip {
	public ExampleTripTraining(Context context) {
		super(context, R.string.example_training_name, R.string.empty_string);
	}
	
	@Override
	public Long getId() {
		return -2L;
	}
	
	@Override
	public float getDistance() {
		return 87936.99f;
	}
	
	@Override
	public float getTime() {
		return 14519000f;
	}
	
	@Override
	public Float getCurrentSpeed() {
		return 6.83f;
	}
	
	@Override
	public Float getMaxSpeed() {
		return 12.11f;
	}
	
	@Override
	public Float getAltitude() {
		return 257.48f;
	}
	
	@Override
	public float getElevationAsc() {
		return 609.07f;
	}
	
	@Override
	public float getElevationDesc() {
		return 488.84f;
	}
	
	@Override
	public Float getMinAltitude() {
		return 157.55f;
	}
	
	@Override
	public Float getMaxAltitude() {
		return 429.91f;
	}
	
	@Override
	public float getTotalTime() {
		return 20319000f;
	}
	
	@Override
	public Float getBearing() {
		return 45.91f;
	}
	
	@Override
	public Float getSlope() {
		return -7.37f;
	}
	
	@Override
	public Float getPowerFactor() {
		return 34.09f;
	}
	
	@Override
	public Float getInitialAltitude() {
		return 215.09f;
	}
	
	@Override
	public Long getStartTime() {
		return dateFromString("29.09.2013 08:10:33");
	}
	
	@Override
	public Long getEndTime() {
		return dateFromString("29.09.2013 14:32:50");
	}
}
