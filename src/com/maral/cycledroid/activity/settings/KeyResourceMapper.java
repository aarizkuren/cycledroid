/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.activity.settings;

import java.lang.ref.WeakReference;

import com.maral.cycledroid.R;
import com.maral.cycledroid.activity.settings.Settings.SettingType;
import com.maral.cycledroid.mapper.ResourceMapper;

final class KeyResourceMapper implements ResourceMapper<SettingType> {
	private static WeakReference<KeyResourceMapper> instance = new WeakReference<KeyResourceMapper>(null);
	
	private KeyResourceMapper() {}
	
	public static KeyResourceMapper getInstance() {
		KeyResourceMapper reference = instance.get();
		if(reference == null) {
			reference = new KeyResourceMapper();
			instance = new WeakReference<KeyResourceMapper>(reference);
		}
		return reference;
	}
	
	public int getResource(SettingType setting) {
		switch(setting) {
			case ALTITUDE_UNIT:
				return R.string.key_altitude_unit;
			case AUTO_START_TRACKING:
				return R.string.key_auto_start_tracking;
			case CALORIES_AGE:
				return R.string.key_calories_age;
			case CALORIES_BIRTH_YEAR:
				return R.string.key_calories_birth_year;
			case CALORIES_SEX:
				return R.string.key_calories_sex;
			case CALORIES_WEIGHT:
				return R.string.key_calories_weight;
			case DISTANCE_UNIT:
				return R.string.key_distance_unit;
			case GRAPH_ALTITUDE_ZERO:
				return R.string.key_graph_altitude_zero;
			case LOCK_SCREEN:
				return R.string.key_lock_screen;
			case ORIENTATION:
				return R.string.key_orientation;
			case PACE_UNIT:
				return R.string.key_pace_unit;
			case REMOVE_DONATE:
				return R.string.key_remove_donate;
			case SPEED_UNIT:
				return R.string.key_speed_unit;
			case TRIPS_SORTING:
				return R.string.key_trips_sorting;
			case UNITS_SYSTEM:
				return R.string.key_units_system;
			case VOLUME_UNIT:
				return R.string.key_volume_unit;
			case WEIGHT_UNIT:
				return R.string.key_weight_unit;
			case FACEBOOK_CALORIES:
				return R.string.key_facebook_calories;
			case FACEBOOK_DURATION:
				return R.string.key_facebook_duration;
			case FACEBOOK_MAP:
				return R.string.key_facebook_map;
			case FACEBOOK_PACE:
				return R.string.key_facebook_pace;
			case FACEBOOK_SPEED:
				return R.string.key_facebook_speed;
			case FACEBOOK_TRIP_DATE:
				return R.string.key_facebook_trip_date;
		}
		throw new IllegalArgumentException("Setting type " + setting.name() + " is not supported.");
	}
}
