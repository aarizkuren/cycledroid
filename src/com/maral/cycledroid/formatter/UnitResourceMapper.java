/**
 * Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall
 * 
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

package com.maral.cycledroid.formatter;

import java.lang.ref.WeakReference;

import com.maral.cycledroid.R;
import com.maral.cycledroid.mapper.ResourceMapper;
import com.maral.cycledroid.model.Unit.Altitude;
import com.maral.cycledroid.model.Unit.Angle;
import com.maral.cycledroid.model.Unit.Distance;
import com.maral.cycledroid.model.Unit.Energy;
import com.maral.cycledroid.model.Unit.Pace;
import com.maral.cycledroid.model.Unit.Power;
import com.maral.cycledroid.model.Unit.Speed;
import com.maral.cycledroid.model.Unit.Volume;
import com.maral.cycledroid.model.Unit.Weight;

final class UnitResourceMapper {
	private UnitResourceMapper() {} // prevent from creating
	
	public static final class AltitudeMapper implements ResourceMapper<Altitude> {
		private static WeakReference<AltitudeMapper> instance = new WeakReference<AltitudeMapper>(null);
		
		private AltitudeMapper() {}
		
		public static AltitudeMapper getInstance() {
			AltitudeMapper reference = instance.get();
			if(reference == null) {
				reference = new AltitudeMapper();
				instance = new WeakReference<AltitudeMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Altitude unit) {
			switch(unit) {
				case FT:
					return R.string.unit_altitude_ft_abbr;
				case KM:
					return R.string.unit_altitude_km_abbr;
				case M:
					return R.string.unit_altitude_m_abbr;
				case MI:
					return R.string.unit_altitude_mi_abbr;
				case NMI:
					return R.string.unit_altitude_nmi_abbr;
			}
			throw new IllegalArgumentException("Altitude unit " + unit + " is not supported.");
		}
	}
	
	public static final class AngleMapper implements ResourceMapper<Angle> {
		private static WeakReference<AngleMapper> instance = new WeakReference<AngleMapper>(null);
		
		private AngleMapper() {}
		
		public static AngleMapper getInstance() {
			AngleMapper reference = instance.get();
			if(reference == null) {
				reference = new AngleMapper();
				instance = new WeakReference<AngleMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Angle unit) {
			switch(unit) {
				case DEGREE:
					return R.string.unit_angle_degree_abbr;
				case PERCENT:
					return R.string.unit_angle_percent_abbr;
			}
			throw new IllegalArgumentException("Angle unit " + unit + " is not supported.");
		}
	}
	
	public static final class DistanceMapper implements ResourceMapper<Distance> {
		private static WeakReference<DistanceMapper> instance = new WeakReference<DistanceMapper>(null);
		
		private DistanceMapper() {}
		
		public static DistanceMapper getInstance() {
			DistanceMapper reference = instance.get();
			if(reference == null) {
				reference = new DistanceMapper();
				instance = new WeakReference<DistanceMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Distance unit) {
			switch(unit) {
				case FT:
					return R.string.unit_distance_ft_abbr;
				case KM:
					return R.string.unit_distance_km_abbr;
				case M:
					return R.string.unit_distance_m_abbr;
				case MI:
					return R.string.unit_distance_mi_abbr;
				case NMI:
					return R.string.unit_distance_nmi_abbr;
			}
			throw new IllegalArgumentException("Distance unit " + unit + " is not supported.");
		}
	}
	
	public static final class EnergyMapper implements ResourceMapper<Energy> {
		private static WeakReference<EnergyMapper> instance = new WeakReference<EnergyMapper>(null);
		
		private EnergyMapper() {}
		
		public static EnergyMapper getInstance() {
			EnergyMapper reference = instance.get();
			if(reference == null) {
				reference = new EnergyMapper();
				instance = new WeakReference<EnergyMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Energy unit) {
			switch(unit) {
				case KCAL:
					return R.string.unit_energy_kcal_abbr;
			}
			throw new IllegalArgumentException("Energy unit " + unit + " is not supported.");
		}
	}
	
	public static final class PaceMapper implements ResourceMapper<Pace> {
		private static WeakReference<PaceMapper> instance = new WeakReference<PaceMapper>(null);
		
		private PaceMapper() {}
		
		public static PaceMapper getInstance() {
			PaceMapper reference = instance.get();
			if(reference == null) {
				reference = new PaceMapper();
				instance = new WeakReference<PaceMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Pace unit) {
			switch(unit) {
				case S_PER_KM:
					return R.string.unit_pace_tpkm_abbr;
				case S_PER_MI:
					return R.string.unit_pace_tpmi_abbr;
				case S_PER_NMI:
					return R.string.unit_pace_tpnmi_abbr;
				case MS_PER_FT:
					return R.string.unit_pace_tpft_abbr;
				case MS_PER_M:
					return R.string.unit_pace_tpm_abbr;
			}
			throw new IllegalArgumentException("Pace unit " + unit + " is not supported.");
		}
	}
	
	public static final class PowerMapper implements ResourceMapper<Power> {
		private static WeakReference<PowerMapper> instance = new WeakReference<PowerMapper>(null);
		
		private PowerMapper() {}
		
		public static PowerMapper getInstance() {
			PowerMapper reference = instance.get();
			if(reference == null) {
				reference = new PowerMapper();
				instance = new WeakReference<PowerMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Power unit) {
			/*switch(unit) {
				case HP:
					return R.string.unit_power_hp_abbr;
				case W:
					return R.string.unit_power_w_abbr;
			}*/ // not supported yet
			throw new IllegalArgumentException("Power unit " + unit + " is not supported.");
		}
	}
	
	public static final class SpeedMapper implements ResourceMapper<Speed> {
		private static WeakReference<SpeedMapper> instance = new WeakReference<SpeedMapper>(null);
		
		private SpeedMapper() {}
		
		public static SpeedMapper getInstance() {
			SpeedMapper reference = instance.get();
			if(reference == null) {
				reference = new SpeedMapper();
				instance = new WeakReference<SpeedMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Speed unit) {
			switch(unit) {
				case FTS:
					return R.string.unit_speed_fts_abbr;
				case KMH:
					return R.string.unit_speed_kmh_abbr;
				case KN:
					return R.string.unit_speed_kn_abbr;
				case MPH:
					return R.string.unit_speed_mph_abbr;
				case MS:
					return R.string.unit_speed_ms_abbr;
			}
			throw new IllegalArgumentException("Speed unit " + unit + " is not supported.");
		}
	}
	
	public static final class VolumeMapper implements ResourceMapper<Volume> {
		private static WeakReference<VolumeMapper> instance = new WeakReference<VolumeMapper>(null);
		
		private VolumeMapper() {}
		
		public static VolumeMapper getInstance() {
			VolumeMapper reference = instance.get();
			if(reference == null) {
				reference = new VolumeMapper();
				instance = new WeakReference<VolumeMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Volume unit) {
			switch(unit) {
				case GAL:
					return R.string.unit_volume_gal_abbr;
				case L:
					return R.string.unit_volume_l_abbr;
			}
			throw new IllegalArgumentException("Volume unit " + unit + " is not supported.");
		}
	}
	
	public static final class WeightMapper implements ResourceMapper<Weight> {
		private static WeakReference<WeightMapper> instance = new WeakReference<WeightMapper>(null);
		
		private WeightMapper() {}
		
		public static WeightMapper getInstance() {
			WeightMapper reference = instance.get();
			if(reference == null) {
				reference = new WeightMapper();
				instance = new WeakReference<WeightMapper>(reference);
			}
			return reference;
		}
		
		public int getResource(Weight unit) {
			switch(unit) {
				case G:
					return R.string.unit_weight_g_abbr;
				case OZ:
					return R.string.unit_weight_oz_abbr;
			}
			throw new IllegalArgumentException("Weight unit " + unit + " is not supported.");
		}
	}
}
