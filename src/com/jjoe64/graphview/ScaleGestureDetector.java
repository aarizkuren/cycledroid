package com.jjoe64.graphview;

import java.lang.reflect.Constructor;
import java.lang.reflect.Method;

import android.content.Context;
import android.view.MotionEvent;

/**
 * Copyright (C) 2011 Jonas Gehring Licensed under the GNU Lesser General Public License (LGPL)
 * http://www.gnu.org/licenses/lgpl.html
 */
public class ScaleGestureDetector {
	public static interface SimpleOnScaleGestureListener {
		boolean onScale(ScaleGestureDetector detector);
	}
	
	private Object realScaleGestureDetector = null;
	private Method methodGetScaleFactor = null;
	private Method methodIsInProgress = null;
	private Method methodOnTouchEvent = null;
	
	public ScaleGestureDetector(Context context, SimpleOnScaleGestureListener simpleOnScaleGestureListener) {
		try {
			// check if class is available
			Class.forName("android.view.ScaleGestureDetector");
			
			// load class and methods
			Class<?> classRealScaleGestureDetector = Class.forName("com.jjoe64.graphview.RealScaleGestureDetector");
			methodGetScaleFactor = classRealScaleGestureDetector.getMethod("getScaleFactor");
			methodIsInProgress = classRealScaleGestureDetector.getMethod("isInProgress");
			methodOnTouchEvent = classRealScaleGestureDetector.getMethod("onTouchEvent", MotionEvent.class);
			
			// create real ScaleGestureDetector
			Constructor<?> constructor = classRealScaleGestureDetector.getConstructor(Context.class, getClass(),
					SimpleOnScaleGestureListener.class);
			realScaleGestureDetector = constructor.newInstance(context, this, simpleOnScaleGestureListener);
		} catch(Exception exception) {}
	}
	
	public double getScaleFactor() {
		if(methodGetScaleFactor != null) {
			try {
				return (Float)methodGetScaleFactor.invoke(realScaleGestureDetector);
			} catch(Exception exception) {
				return 1.0;
			}
		}
		return 1.0;
	}
	
	public boolean isInProgress() {
		if(methodGetScaleFactor != null) {
			try {
				return (Boolean)methodIsInProgress.invoke(realScaleGestureDetector);
			} catch(Exception exception) {}
		}
		return false;
	}
	
	public void onTouchEvent(MotionEvent event) {
		if(methodOnTouchEvent != null) {
			try {
				methodOnTouchEvent.invoke(realScaleGestureDetector, event);
			} catch(Exception exception) {}
		}
	}
}
