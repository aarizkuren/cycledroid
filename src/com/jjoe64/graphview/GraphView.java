package com.jjoe64.graphview;

import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import android.annotation.SuppressLint;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Paint.Align;
import android.graphics.Path;
import android.util.AttributeSet;
import android.view.MotionEvent;
import android.view.View;
import android.widget.LinearLayout;

import com.maral.cycledroid.R;

/**
 * Graph View. This draws a line chart.
 * 
 * @author jjoe64 - jonas gehring - http://www.jjoe64.com
 * 
 *         Copyright (C) 2011 Jonas Gehring Licensed under the GNU Lesser General Public License (LGPL)
 *         http://www.gnu.org/licenses/lgpl.html
 */
public class GraphView extends LinearLayout implements Comparator<GraphViewData> {
	private static final int MAX_DATA = 50; // per width = 100
	private static final int TRANSPARENCY_MASK = 0x90; // for background below the graph
	
	private float border;
	private float verticalLabelWidth;
	private float horizontalLabelHeight;
	
	// view won't refresh on call to invalidate() with hardware acceleration enabled on Android 4.0
	private static void reflectDisableHardwareAcceleration(View view) {
		try {
			Class<?> classView = Class.forName("android.view.View");
			Method setLayerType = classView.getMethod("setLayerType", int.class, Paint.class);
			Field layerTypeSoftware = classView.getField("LAYER_TYPE_SOFTWARE");
			setLayerType.invoke(view, layerTypeSoftware.get(null), null);
		} catch(Exception exception) {}
	}
	
	private class ContentView extends View {
		private float lastTouchEventX;
		private float graphWidth;
		
		public ContentView(Context context) {
			super(context);
			setLayoutParams(new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT));
		}
		
		@Override
		protected void onDraw(Canvas canvas) {
			if(!canBeDrawn())
				return;
			
			float horizontalStart = 0.0f;
			float height = getHeight();
			graphWidth = getWidth() - 1.0f;
			
			float graphHeight = height - (2.0f * border);
			
			if(horizontalLabels == null)
				horizontalLabels = generateHorizontalLabels(graphWidth);
			if(verticalLabels == null)
				verticalLabels = generateVerticalLabels(graphHeight);
			
			// vertical lines
			int vers = verticalLabels.length - 1;
			for(int i = 0; i < verticalLabels.length; ++i) {
				float y = ((graphHeight / vers) * i) + border;
				canvas.drawLine(horizontalStart, y, graphWidth, y, linesPaint);
			}
			
			// horizontal labels + lines
			for(int i = 0; i < horizontalLabels.length; ++i) {
				float x = ((graphWidth / (horizontalLabels.length - 1)) * i) + horizontalStart;
				canvas.drawLine(x, height - border, x, border, linesPaint);
				if(i == 0)
					labelsPaint.setTextAlign(Align.LEFT);
				else if(i == horizontalLabels.length - 1)
					labelsPaint.setTextAlign(Align.RIGHT);
				else
					labelsPaint.setTextAlign(Align.CENTER);
				canvas.drawText(horizontalLabels[i], x, height - 4.0f, labelsPaint);
			}
			
			if(getMaxY() != getMinY())
				drawSeries(canvas, graphWidth, graphHeight, horizontalStart);
		}
		
		private void onMoveGesture(float f) {
			// view port update
			viewportStart -= f * viewportSize / graphWidth;
			
			// minimal and maximal view limit
			float minX = getMinX(true);
			float maxX = getMaxX(true);
			if(viewportStart < minX)
				viewportStart = minX;
			else if(viewportStart + viewportSize > maxX)
				viewportStart = maxX - viewportSize;
			
			refresh();
		}
		
		@SuppressLint("ClickableViewAccessibility")
		@Override
		public boolean onTouchEvent(MotionEvent event) {
			if(!scalable)
				return super.onTouchEvent(event);
			
			boolean handled = false;
			if(scalable && scaleDetector != null) { // first scale
				scaleDetector.onTouchEvent(event);
				handled = scaleDetector.isInProgress();
			}
			if(!handled) { // if not scaled, scroll
				if((event.getAction() & MotionEvent.ACTION_DOWN) == MotionEvent.ACTION_DOWN)
					handled = true;
				if((event.getAction() & MotionEvent.ACTION_UP) == MotionEvent.ACTION_UP) {
					lastTouchEventX = 0.0f;
					handled = true;
				}
				if((event.getAction() & MotionEvent.ACTION_MOVE) == MotionEvent.ACTION_MOVE) {
					if(lastTouchEventX != 0.0f)
						onMoveGesture(event.getX() - lastTouchEventX);
					lastTouchEventX = event.getX();
					handled = true;
				}
			}
			return handled;
		}
	}
	
	private class VerticalLabelsView extends View {
		private static final int SPACE = 2;
		
		private float width = 0.0f;
		private LayoutParams params = new LayoutParams(0, LayoutParams.FILL_PARENT);
		
		public VerticalLabelsView(Context context) {
			super(context);
		}
		
		@Override
		protected void onDraw(Canvas canvas) {
			if(!canBeDrawn())
				return;
			
			float height = getHeight();
			float graphheight = height - (2.0f * border);
			
			if(verticalLabels == null)
				verticalLabels = generateVerticalLabels(graphheight);
			
			// vertical labels
			labelsPaint.setTextAlign(Align.LEFT);
			width = 0.0f;
			for(int i = 0; i < verticalLabels.length; ++i) {
				float y = ((graphheight / (verticalLabels.length - 1)) * i) + border;
				canvas.drawText(verticalLabels[i], 0.0f, y, labelsPaint);
				float labelWidth = labelsPaint.measureText(verticalLabels[i]);
				if(labelWidth > width)
					width = labelWidth;
			}
			
			params.width = (int)Math.ceil(width) + SPACE;
			setLayoutParams(params); // avoid memory allocation in onDraw
		}
	}
	
	private boolean scalable = false;
	private ScaleGestureDetector scaleDetector = null;
	
	private String[] verticalLabels = null;
	private String[] horizontalLabels = null;
	private LabelFormatter xFormatter = null;
	private LabelFormatter yFormatter = null;
	private VerticalLabelsView verticalLabelsView;
	private final Paint labelsPaint = new Paint();
	
	private List<GraphViewData> data = null;
	private int futureCount = 0;
	private final Paint graphStrokePaint = new Paint();
	private final Paint graphBelowPaint = new Paint();
	private final Paint linesPaint = new Paint();
	private final Path path = new Path();
	
	private float viewportStart;
	private float viewportSize;
	private boolean manualMinYAxis = false;
	private boolean manualMaxYAxis = false;
	private float realMinYValue;
	private float manualMinYValue;
	private float realMaxYValue;
	private float manualMaxYValue;
	
	public GraphView(Context context) {
		super(context);
		initialize();
	}
	
	public GraphView(Context context, AttributeSet attrs) {
		super(context, attrs);
		initialize();
	}
	
	private void initialize() {
		reflectDisableHardwareAcceleration(this);
		Resources resources = getContext().getResources();
		
		graphStrokePaint.setStrokeCap(Paint.Cap.ROUND);
		graphStrokePaint.setStrokeWidth(resources.getDimension(R.dimen.graph_stroke));
		graphStrokePaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		graphStrokePaint.setStyle(Paint.Style.STROKE);
		graphBelowPaint.setStyle(Paint.Style.FILL);
		labelsPaint.setColor(resources.getColor(R.color.graph_labels));
		labelsPaint.setFlags(Paint.ANTI_ALIAS_FLAG);
		labelsPaint.setTextSize(resources.getDimension(R.dimen.graph_label_text_size));
		linesPaint.setColor(resources.getColor(R.color.graph_lines));
		
		border = resources.getDimension(R.dimen.graph_border);
		verticalLabelWidth = resources.getDimension(R.dimen.graph_vertical_label);
		horizontalLabelHeight = resources.getDimension(R.dimen.graph_horizontal_label);
		
		verticalLabelsView = new VerticalLabelsView(getContext());
		addView(verticalLabelsView);
		LayoutParams params = new LayoutParams(LayoutParams.FILL_PARENT, LayoutParams.FILL_PARENT, 1);
		addView(new ContentView(getContext()), params);
	}
	
	public void setData(List<GraphViewData> data) {
		this.data = data;
	}
	
	public void setFutureCount(int futureCount) {
		this.futureCount = futureCount;
	}
	
	public void setColor(int color) {
		graphStrokePaint.setColor(color);
		graphBelowPaint.setColor((color & (~0 >>> 8)) | (TRANSPARENCY_MASK << 24));
	}
	
	private void calculateRealYBounds() {
		realMinYValue = Float.POSITIVE_INFINITY;
		realMaxYValue = Float.NEGATIVE_INFINITY;
		int size = data.size();
		for(int i = 0; i < size; ++i) {
			GraphViewData current = data.get(i);
			if(current.valueY < realMinYValue)
				realMinYValue = current.valueY;
			if(current.valueY > realMaxYValue)
				realMaxYValue = current.valueY;
		}
	}
	
	public void setViewport(float viewportStart, float viewportSize) {
		this.viewportStart = viewportStart;
		this.viewportSize = viewportSize;
	}
	
	private void drawSeries(Canvas canvas, float graphWidth, float graphHeight, float horizontalStart) {
		int dataSize = data.size(); // collection may expand during drawing
		if(dataSize == 0)
			return;
		
		float minX = getMinX(false);
		float diffX = getMaxX(false) - minX;
		float minY = getMinY();
		float diffY = getMaxY() - minY;
		float lastEndY = 0.0f;
		float lastEndX = 0.0f;
		
		int start = Collections.binarySearch(data, new GraphViewData(minX, 0.0f), this);
		if(start < 0)
			start = -start - 2; // see what binary search returns
		int end = 0;
		if(data.get(dataSize - 1).valueX < minX + diffX) // collection probably not yet full (dataSize < futureCount)
			end = futureCount - 1;
		if(end < dataSize - 1)
			end = Collections.binarySearch(data, new GraphViewData(minX + diffX, 0.0f), this);
		if(end < 0)
			end = -end;
		int divide = (int)(MAX_DATA * graphWidth / 100.0f);
		int move = 1 + (divide != 0? (end - start) / divide : 0);
		start -= start % move;
		end += (move - end % move);
		
		for(int i = start; i <= end; i += move) {
			if(i < 0 || i >= dataSize)
				continue;
			
			float x = graphWidth * (data.get(i).valueX - minX) / diffX;
			if(Math.abs(lastEndX - x) < 0.01 && lastEndX != 0.0)
				continue; // avoid straight vertical lines
			float y = graphHeight * (data.get(i).valueY - minY) / diffY;
			
			if(i != start) {
				float startX = lastEndX + (horizontalStart + 1.0f);
				float startY = (border - lastEndY) + graphHeight;
				float endX = x + (horizontalStart + 1.0f);
				float endY = (border - y) + graphHeight;
				canvas.drawLine(startX, startY, endX, endY, graphStrokePaint);
				path.reset();
				path.moveTo(startX, startY);
				path.lineTo(endX, endY);
				path.lineTo(endX, border + graphHeight);
				path.lineTo(startX, border + graphHeight);
				canvas.drawPath(path, graphBelowPaint);
			}
			
			lastEndY = y;
			lastEndX = x;
		}
	}
	
	private String[] generateHorizontalLabels(float graphWidth) {
		int numLabels = (int)(graphWidth / verticalLabelWidth);
		String[] labels = new String[numLabels + 1];
		float minX = getMinX(false);
		float maxY = getMaxX(false);
		for(int i = 0; i <= numLabels; ++i)
			labels[i] = xFormatter.formatLabel(minX + ((maxY - minX) * i / numLabels));
		return labels;
	}
	
	private String[] generateVerticalLabels(float graphHeight) {
		int numLabels = (int)(graphHeight / horizontalLabelHeight);
		String[] labels = new String[numLabels + 1];
		float minY = getMinY();
		float maxY = getMaxY();
		for(int i = 0; i <= numLabels; ++i)
			labels[numLabels - i] = yFormatter.formatLabel(minY + ((maxY - minY) * i / numLabels));
		return labels;
	}
	
	private float getMinX(boolean ignoreViewport) {
		return ignoreViewport? data.get(0).valueX : viewportStart;
	}
	
	private float getMaxX(boolean ignoreViewport) {
		return ignoreViewport? data.get(data.size() - 1).valueX : viewportStart + viewportSize;
	}
	
	private float getMinY() {
		return manualMinYAxis? manualMinYValue : realMinYValue;
	}
	
	private float getMaxY() {
		return manualMaxYAxis? manualMaxYValue : realMaxYValue;
	}
	
	public void setManualMinYAxis(boolean manualMinYAxis) {
		this.manualMinYAxis = manualMinYAxis;
	}
	
	public void setManualMaxYAxis(boolean manualMaxYAxis) {
		this.manualMaxYAxis = manualMaxYAxis;
	}
	
	public void setManualMinYAxisBound(float manualMinYValue) {
		this.manualMinYValue = manualMinYValue;
		manualMinYAxis = true;
	}
	
	public void setManualMaxYAxisBound(float manualMaxYValue) {
		this.manualMaxYValue = manualMaxYValue;
		manualMaxYAxis = true;
	}
	
	public void setScalable(boolean scalable) {
		this.scalable = scalable;
		if(!scalable || scaleDetector != null)
			return;
		
		ScaleGestureDetector.SimpleOnScaleGestureListener mediator = new ScaleGestureDetector.SimpleOnScaleGestureListener() {
			public boolean onScale(ScaleGestureDetector detector) {
				float newSize = viewportSize * (float)detector.getScaleFactor();
				float diff = newSize - viewportSize;
				if(diff > viewportSize)
					diff = 0.0f; // because of some problems
				viewportStart += diff / 2.0;
				viewportSize -= diff;
				
				if(diff < 0) {
					if(viewportStart < getMinX(true))
						viewportStart = getMinX(true);
					
					float overlap = viewportStart + viewportSize - getMaxX(true);
					if(overlap > 0.0) {
						// scroll left
						if(viewportStart - overlap > getMinX(true))
							viewportStart -= overlap;
						else {
							// maximal scale
							viewportStart = getMinX(true);
							viewportSize = getMaxX(true) - viewportStart;
						}
					}
				}
				
				refresh();
				return true;
			}
		};
		scaleDetector = new ScaleGestureDetector(getContext(), mediator);
	}
	
	public float getViewportStart() {
		return viewportStart;
	}
	
	public float getViewportSize() {
		return viewportSize;
	}
	
	public int compare(GraphViewData lhs, GraphViewData rhs) {
		if(lhs.valueX < rhs.valueX)
			return -1;
		if(lhs.valueX > rhs.valueX)
			return 1;
		return 0;
	}
	
	public void setFormatters(LabelFormatter xFormatter, LabelFormatter yFormatter) {
		this.xFormatter = xFormatter;
		this.yFormatter = yFormatter;
	}
	
	private boolean canBeDrawn() {
		return data != null && xFormatter != null && yFormatter != null;
	}
	
	public void refresh() {
		if(canBeDrawn()) {
			verticalLabels = null;
			horizontalLabels = null;
			calculateRealYBounds();
			invalidate();
			verticalLabelsView.invalidate();
		}
	}
}
