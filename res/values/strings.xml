<?xml version="1.0" encoding="utf-8"?>

<!--
Copyright (C) 2011, 2012, 2013, 2014  Michał Marschall

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
-->

<resources>
	<string name="app_name_free">CycleDroid (free)</string>
	<string name="app_name_donate">CycleDroid (donate)</string>
	
	<!-- Common buttons and menu items -->
	<string name="add">Add</string>
	<string name="cancel">Cancel</string>
	<string name="delete">Delete</string>
	<string name="donate">Donate</string>
	<string name="edit">Edit</string>
	<string name="no">No</string>
	<string name="ok">OK</string>
	<string name="open">Open</string>
	<string name="refresh">Refresh</string>
	<string name="save">Save</string>
	<string name="settings">Settings</string>
	<string name="yes">Yes</string>
	<string name="close">Close</string>
	<string name="help">Help</string>
	<string name="no_data_help"><u>Help</u></string>
	<string name="error">Error</string>
	<string name="toast_saved_file">Saved to the file <b><i>%1$s</i></b>.</string>
	<string name="progress_loading_data">Loading data. Please wait&#8230;</string>

	<!-- TripActivity -->
	<string name="share_on_facebook">Share on Facebook</string>
	<string name="export_data">Export</string>
	<string name="map">Map</string>
	<string name="auto_change">Auto scroll</string>
	<string name="copy_to_clipboard">Copy to clipboard</string>
	<string name="copied_to_clipboard">All parameters have been copied to the clipboard.</string>
	<string name="trips_summary_name">Summary</string>
	<string name="satellites_count">Satellites:</string>
	<string name="status_acquiring">Acquiring location</string>
	<string name="status_fix">Tracking is active</string>
	<string name="status_gps_disabled">GPS disabled</string>
	<string name="status_no_fix">No GPS signal</string>
	<string name="status_service_disabled">Tracking disabled</string>
	<string name="distance_altitude">altitude/distance</string>
	<string name="distance_speed">speed/distance</string>
	<string name="time_speed">speed/time</string>
	<string name="altitude">altitude</string>
	<string name="average_speed">average speed</string>
	<string name="current_speed">current speed</string>
	<string name="distance">distance</string>
	<string name="max_speed">maximum speed</string>
	<string name="time">ride time</string>
	<string name="elevation_asc">asc. elevation</string>
	<string name="elevation_desc">desc. elevation</string>
	<string name="max_altitude">maximum altitude</string>
	<string name="min_altitude">minimum altitude</string>
	<string name="total_time">total time</string>
	<string name="calories">burned calories</string>
	<string name="fat_burned">burned fat</string>
	<string name="oxygen_consumed">consumed oxygen</string>
	<string name="bearing">bearing</string>
	<string name="pace_netto">net pace</string>
	<string name="pace_brutto">gross pace</string>
	<string name="brutto_speed">gross speed</string>
	<string name="slope">slope</string>
	<string name="initial_altitude">initial altitude</string>
	<string name="final_altitude">final altitude</string>
	<string name="start_time">start time</string>
	<string name="end_time">end time</string>
	<string name="time_from_start">time from start</string>
	<string name="content_description_graphs">Graphs</string>
	<string name="content_description_pause">Stop tracking</string>
	<string name="content_description_play">Start tracking</string>
	<string name="confirm_title_close_trip">Quit</string>
	<string name="confirm_message_close_trip">If you close this view, data will no longer be recorded. To avoid this, use the home button. Are you sure you want to quit?</string>
	<string name="dialog_auto_change_message">Automatically scroll a displayed page with interval (in seconds):</string>
	<string name="dialog_export_format">Select a destination format:</string>
	<string name="list_select_type">Select a graph type:</string>
	<string name="progress_exporting">Exporting data. Please wait&#8230;</string>
	<plurals name="toast_auto_scroll_start">
		<item quantity="one">A view will be automatically changed every %1$d second. To stop, touch it anywhere.</item>
		<item quantity="other">A view will be automatically changed every %1$d seconds. To stop, touch it anywhere.</item>
	</plurals>
	<string name="toast_auto_scroll_stop">Auto scrolling has been disabled.</string>
	<string name="toast_auto_start_tracking">Tracking has been automatically enabled. You can change this behavior in the application settings.</string>
	<string name="toast_export_failed">While exporting to the file <b><i>%1$s</i></b>, an error has occurred. Check whether there is enough free space and you have permissions to save files in the selected location.</string>
	<string name="toast_gps_disabled">GPS is disabled. To start tracking, enable it in the device settings.</string>
	<string name="toast_move_screen">The parameters are placed on four pages. Scroll this view horizontally in any direction to see other parameters.</string>
	<string name="alert_message_facebook_empty">No data to publish. Collect data, enabling tracking.</string>
	<string name="alert_message_facebook_unsupported">To use this feature, you must install the latest version of the Facebook application for Android from Play Store.</string>

	<!-- TripsListActivity -->
	<string name="label_trips_list">Trips</string>
	<string name="import_data">Import from CSV</string>
	<string name="summary">Summary</string>
	<string name="no_trips">No trips. To add a new one, press the button below.</string>
	<string name="alert_message_no_trips">No trips.</string>
	<string name="confirm_message_delete_trip">Are you sure you want to delete the selected trip? All data associated with it will be irretrievably lost.</string>
	<string name="progress_importing">Importing data. Please wait&#8230;</string>
	<string name="progress_updating_database">The new version has been installed. Updating database. This may take a few minutes&#8230;</string>
	<string name="progress_deleting_trip">Deleting the trip. Please wait&#8230;</string>
	<string name="toast_all_imported">All trips have been successfully imported.</string>
	<string name="toast_not_all_imported">An error has occurred. %1$d/%2$d trips have been successfully imported. Check whether all the files are correct and were not created with a newer version of the application.</string>
	<string name="toast_trip_imported">The trip <b><i>%1$s</i></b> from the file <b><i>%2$s</i></b> has been successfully imported.</string>
	<string name="toast_trip_not_imported">While importing from the file <b><i>%1$s</i></b>, an error has occurred.</string>

	<!-- MultiDeleteActivity -->
	<string name="label_multi_delete">Delete trips</string>
	<string name="confirm_title_delete_trips">Confirm deleting</string>
	<string name="confirm_message_delete_trips">Are you sure you want to delete the selected trips? All data associated with them will be irretrievably lost.</string>
	<string name="progress_deleting_trips">Deleting trips. Please wait&#8230;</string>

	<!-- MultiExportActivity -->
	<string name="label_multi_export">Export trips</string>
	<string name="progress_exporting_trips">Exporting trips. Please wait&#8230;</string>
	<string name="toast_all_exported">All the selected trips have been successfully exported to the directory <b><i>%1$s</i></b>.</string>
	<string name="toast_not_all_exported">An error has occurred. %1$d/%2$d trips have been successfully exported to the directory <b><i>%3$s</i></b>. Check whether there is enough free space and you have permissions to save files in the selected location.</string>
	<string name="toast_trip_exported">The trip <b><i>%1$s</i></b> has been successfully exported to the file <b><i>%2$s</i></b>.</string>
	<string name="toast_trip_not_exported">While exporting the trip <b><i>%1$s</i></b> to the file <b><i>%2$s</i></b>, an error has occurred.</string>

	<!-- SummarySelectActivity -->
	<string name="label_summary_select">Select trips</string>
	<string name="show_summary">Show summary</string>
	<string name="select_all">Select all</string>
	<string name="summary_first_hint">Select trips and press the button below to open their summary.</string>
	<string name="alert_message_nothing_chosen">Select at least one trip.</string>

	<!-- TripEditActivity -->
	<string name="label_create_trip">New trip</string>
	<string name="new_trip">Trip %1$s</string>
	<string name="name">Name:</string>
	<string name="name_hint">Name</string>
	<string name="name_help">Enter a name of this trip.</string>
	<string name="description">Description:</string>
	<string name="description_hint">Description</string>
	<string name="description_help">Enter a short description of this trip. This field is optional and you can leave it blank.</string>
	<string name="alert_message_enter_name">You have to fill in the field with name.</string>
	<string name="confirm_title_edit_close">Quit</string>
	<string name="confirm_message_edit_close">Are you sure you want to quit without saving changes?</string>

	<!-- ChooseFileActivity -->
	<string name="label_file_dialog_save">Save file</string>
	<string name="label_directory_dialog_save">Select directory</string>
	<string name="label_file_dialog_open">Open file</string>
	<string name="label_file_dialog_open_multiple">Open files</string>
	<string name="location">Location</string>
	<string name="filename_hint">Filename</string>
	<string name="directory_name_hint">Directory name</string>
	<string name="illegal_characters">A filename cannot contain the slash character (/).</string>
	<string name="illegal_replaced">Illegal characters in the filename (/) have been replaced by hyphens (-).</string>
	<string name="no_files">No files to display.</string>
	<string name="alert_message_choose_directory">You must select a directory.</string>
	<string name="alert_message_cant_read">The directory cannot be read.</string>
	<string name="alert_message_incorrect_filename">The selected filename is incorrect &#8211; make sure that a directory with this name does not exist.</string>
	<string name="confirm_message_overwrite">A file with the selected name already exists. Do you want to overwrite it?</string>
	<string name="confirm_message_directory_overwrite">The selected directory exists and is not empty. If you use it, the files inside it may be overwritten. Are you sure you want to use the selected directory?</string>
	<string name="toast_choose_directory">Select a directory.</string>
	<string name="content_description_icon">File icon</string>

	<!-- MapActivity -->
	<string name="label_map">%1$s &#8211; map</string>
	<string name="map_service_disabled">Tracking is disabled. Enable it on the trip screen.</string>
	<string name="map_no_location">The current location has not yet been determined.</string>
	<string name="map_follow_no_location">Following enabled, but the current location has not yet been determined.</string>
	<string name="route_start">Start of the route</string>
	<string name="route_end">End of the route</string>
	<string name="current_position">Current position</string>
	<string name="following_disabled">Following has been disabled.</string>
	<string name="follow">Follow</string>
	<string name="menu_location">Location</string>
	<string name="show_route">Show route</string>
	<string name="map_rotate">Rotate map</string>
	<string name="legal_info">Legal notices</string>
	<string name="map_type">Map view</string>
	<string name="select_map_type">Select a map view:</string>
	<string name="map_type_normal">normal</string>
	<string name="map_type_satellite">satellite</string>
	<string name="map_type_hybrid">hybrid</string>
	<string name="map_type_terrain">terrain</string>
	<string name="alert_message_no_play_services">Google Play services needed to display maps are not installed on your phone or they are out of date. Install or update Google Play services from Play Store.</string>

	<!-- GraphActivity -->
	<string name="label_graph_distance_altitude">%1$s &#8211; altitude/distance</string>
	<string name="label_graph_distance_speed">%1$s &#8211; speed/distance</string>
	<string name="label_graph_time_speed">%1$s &#8211; speed/time</string>
	<string name="save_graph">Save as image</string>
	<string name="no_graph_data">No data required to draw a graph. Start tracking and wait until the application starts measuring movement parameters.</string>
	<string name="alert_title_no_graph">No data</string>
	<string name="alert_message_no_graph">Enable tracking and collect data required to draw a graph and then refresh it.</string>
	<string name="toast_save_graph_failed">While saving to the file <b><i>%1$s</i></b>, an error has occurred. Check whether there is enough free space and you have permissions to save files in the selected location.</string>

	<!-- SettingsActivity -->
	<string name="label_settings">Settings</string>
	<string name="category_units">Units</string>
	<string name="category_behaviour">Behavior</string>
	<string name="category_appearance">Appearance</string>
	<string name="category_calories">Calculating calories</string>
	<string name="units_system_title">Unit system</string>
	<string name="units_system_summary">Select a default unit system for the whole application.</string>
	<string name="units_system_imperial_name">imperial (ft, mi, mph, oz, gal)</string>
	<string name="units_system_metric_name">metric (m, km, km/h, g, l)</string>
	<string name="units_system_custom_name">custom</string>
	<string name="units_altitude_title">Altitude</string>
	<string name="units_altitude_summary">Select a default unit in which altitude will be displayed.</string>
	<string name="units_distance_title">Distance</string>
	<string name="units_distance_summary">Select a default unit in which distance will be displayed.</string>
	<string name="units_speed_title">Speed</string>
	<string name="units_speed_summary">Select a default unit in which speed will be displayed.</string>
	<string name="units_weight_title">Mass</string>
	<string name="units_weight_summary">Select a default unit in which mass will be displayed.</string>
	<string name="units_volume_title">Volume</string>
	<string name="units_volume_summary">Select a default unit in which volume will be displayed.</string>
	<string name="units_pace_title">Pace</string>
	<string name="units_pace_summary">Select a default unit in which pace will be displayed.</string>
	<string name="unit_altitude_m_name">meter</string>
	<string name="unit_altitude_km_name">kilometer</string>
	<string name="unit_altitude_ft_name">foot</string>
	<string name="unit_altitude_mi_name">mile</string>
	<string name="unit_altitude_nmi_name">nautical mile</string>
	<string name="unit_speed_ms_name">meter per second</string>
	<string name="unit_speed_kmh_name">kilometer per hour</string>
	<string name="unit_speed_fts_name">foot per second</string>
	<string name="unit_speed_mph_name">mile per hour</string>
	<string name="unit_speed_kn_name">knot</string>
	<string name="unit_weight_g_name">gram</string>
	<string name="unit_weight_oz_name">ounce</string>
	<string name="unit_volume_l_name">liter</string>
	<string name="unit_volume_gal_name">gallon</string>
	<string name="unit_pace_tpkm_name">time/kilometer</string>
	<string name="unit_pace_tpmi_name">time/mile</string>
	<string name="unit_pace_tpnmi_name">time/nautical mile</string>
	<string name="graph_altitude_zero_title">Altitude graphs from zero</string>
	<string name="graph_altitude_zero_summary">Draw altitude graphs starting from the sea level on a vertical axis.</string>
	<string name="lock_screen_title">Lock screen</string>
	<string name="lock_screen_summary">Allow for locking/dimming the screen while tracking is enabled.</string>
	<string name="trips_sorting_title">Sorting trips</string>
	<string name="trips_sorting_summary">Define an order in which trips will be displayed on the list.</string>
	<string name="trips_sorting_alph_asc_name">alphabetically ascending</string>
	<string name="trips_sorting_alph_desc_name">alphabetically descending</string>
	<string name="trips_sorting_chron_asc_name">chronologically ascending</string>
	<string name="trips_sorting_chron_desc_name">chronologically descending</string>
	<string name="remove_donate_title">Remove \"Donate\"</string>
	<string name="remove_donate_summary">Do not show the option \"Donate\" in the options menu.</string>
	<string name="auto_start_tracking_title">Auto start tracking</string>
	<string name="auto_start_tracking_summary">Automatically start tracking after opening a trip.</string>
	<string name="orientation_title">Orientation</string>
	<string name="orientation_summary">Select a screen orientation in the whole application.</string>
	<string name="orientation_portrait_name">portrait</string>
	<string name="orientation_landscape_name">landscape</string>
	<string name="orientation_sensor_name">automatic</string>
	<string name="calories_sex_name">Sex</string>
	<string name="calories_sex_summary">Provide your sex.</string>
	<string name="calories_sex_male_name">male</string>
	<string name="calories_sex_female_name">female</string>
	<string name="calories_weight_name">Weight</string>
	<string name="calories_weight_summary_kg">Provide your weight in kilograms.</string>
	<string name="calories_birth_year_name">Age</string>
	<string name="calories_birth_year_summary">Provide your birth year.</string>
	<string name="category_facebook">Publishing on Facebook</string>
	<string name="facebook_map_title">Include map</string>
	<string name="facebook_map_summary">Post a small map of a trip.</string>
	<string name="facebook_duration_title">Include duration</string>
	<string name="facebook_duration_summary">Post a duration of a trip (a total time without breaks).</string>
	<string name="facebook_speed_title">Include average speed</string>
	<string name="facebook_speed_summary">Post an average net speed of a trip.</string>
	<string name="facebook_calories_title">Include burned calories</string>
	<string name="facebook_calories_summary">Post a total amount of calories burned during a trip. This value will be calculated according to parameters (weight, age, sex) set at a time of publishing a trip.</string>
	<string name="facebook_pace_title">Include pace</string>
	<string name="facebook_pace_summary">Post an average net pace of a trip.</string>
	<string name="facebook_trip_date_title">Trip date on timeline</string>
	<string name="facebook_trip_date_summary">Display a trip on a timeline on a day it was recorded instead of on a day it was published.</string>
	
	<!-- FacebookActivity -->
	<string name="alert_facebook_error_message">Check whether your internet connection is working properly and the latest version of the Facebook application is installed. The error message is:\n<i>%1$s</i></string>
	
	<!-- DonateActivity -->
	<string name="alert_donate_error_message">Cannot use the in-app billing. Check whether the latest version of the Play Store application is installed. The error message is:\n<i>%1$s</i></string>
	<string name="confirm_donate_again_title">Donate again</string>
	<string name="confirm_donate_again_message">You already donated the application. Do you want to do it again?</string>
	<string name="donate_financially_text">CycleDroid is free, but if you like it, you can donate its developer financially. The donation can be done using the in-app purchase with a credit card associated with your Google account.</string>
	<string name="donate_financially_button">Donate financially</string>
	<string name="rate_text">I appreciate rating the application and leaving opinions in comments in Play Store.</string>
	<string name="rate_button">Rate in Play Store</string>
	<string name="translate_text">You can also help by translating CycleDroid to your language on the site: <a href="http://crowdin.net/project/cycledroid">http://crowdin.net/project/cycledroid</a>.</string>
	<string name="translate_button">Translate on Crowdin</string>

	<!-- Help files -->
	<string name="help_file">en/help.html</string>

	<!-- Units -->
	<string name="unit_altitude_m_abbr">m</string>
	<string name="unit_altitude_km_abbr">km</string>
	<string name="unit_altitude_ft_abbr">ft</string>
	<string name="unit_altitude_mi_abbr">mi</string>
	<string name="unit_altitude_nmi_abbr">NM</string>
	<string name="unit_speed_ms_abbr">m/s</string>
	<string name="unit_speed_kmh_abbr">km/h</string>
	<string name="unit_speed_fts_abbr">fts</string>
	<string name="unit_speed_mph_abbr">mph</string>
	<string name="unit_speed_kn_abbr">kn</string>
	<string name="unit_weight_g_abbr">g</string>
	<string name="unit_weight_oz_abbr">oz</string>
	<string name="unit_volume_l_abbr">l</string>
	<string name="unit_volume_gal_abbr">gal</string>
	<string name="unit_energy_kcal_abbr">kcal</string>
	<string name="unit_pace_tpkm_abbr">time/km</string>
	<string name="unit_pace_tpmi_abbr">time/mi</string>
	<string name="unit_pace_tpnmi_abbr">time/NM</string>
	<string name="unit_pace_tpft_abbr">time/ft</string>
	<string name="unit_pace_tpm_abbr">time/m</string>

	<!-- Example trip (for screenshots) -->
	<string name="example_training_name">Training</string>
	<string name="example_lakes_name">Lakes</string>
	<string name="example_city_name">City</string>
	<string name="example_weekend_name">Weekend</string>
	<string name="example_weekend_description">22&#8211;23/06/2013</string>
</resources>