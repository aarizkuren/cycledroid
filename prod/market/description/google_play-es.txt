CycleDroid – ordenador de bicicleta

CycleDroid es una aplicación que le permite rastrear su movimiento mediante GPS y visualizar parámetros tales como: velocidad, distancia, tiempo, altura, elevación, pendiente, calorías quemadas, grasa y otros muchos. La ruta puede ser mostrada en un mapa y publicada en Facebook. Permite crear sesiones y asignarles los datos recogidos en cada una de ellas. También existe la posibilidad de ver un resumen de las rutas seleccionadas: distancia total, tiempo, etc.

Los datos recogidos por la aplicación pueden ser exportados en una tarjeta SD en formato CSV e importados a otro dispositivo con CycleDroid instalado. También es posible guardar los datos en formatos GPX (GPS eXchange) o KML y ver la ruta en un mapa en el PC (por ejemplo en Google Earth).

CycleDroid también proporciona una función de gráficos: altura/distancia, velocidad/distancia, velocidad/tiempo. Podrá acercar o alejar un gráfico tocando la pantalla. La aplicación le permite guardar un gráfico o parte de él como un archivo de imagen.

Permisos:
– ubicación precisa (basada en red y GPS) – para rastreo de la ruta,
– modificar o eliminar contenido del almacenamiento USB, probar acceso a almacenamiento protegido – para la importación y exportación de rutas,
– impedir que el dispositivo entre en modo de suspensión – para mantener el rastreo mientras el dispositivo no se utiliza,
– acceso completo a red, ver conexiones de red, leer la configuración de los servicios de Google – para mapas de Google y Facebook.

No dude en inormar de errores en la aplicación o de sus traducciones, en dejar sus opiniones y en ponerse en contacto conmigo, ya sea directamente o a través del perfil de CycleDroid en Facebook: http://www.facebook.com/cycledroid. Si le gusta esta aplicación, por favor puntúela. Si desea ayudarme y participar en la traducción de CycleDroid, entre en: http://crowdin.net/project/cycledroid.

===================================================

CycleDroid (donar)

Esta versión de la aplicación lleva tiempo sin actualizarse. Hay disponible una versión idéntica y gratuita en Play Store – por favor acceda a ella para descargarla. Si desea hacer una donación por mi trabajo, puede hacer una compra desde la propia app gratuita.

===================================================

Donar CycleDroid

Si le gusta esta aplicación, puede hacer una donación por mi trabajo.

===================================================


– versión turca,
– versión española – gracias a Francisco M. Román por la traducción,
– versión bosnio – gracias a Ishak Vehab por la traducción,
– gráficos más fluidos,
– corrección de errores,
– mejoras en las traducciones de menor importancia.
