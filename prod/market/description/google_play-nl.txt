CycleDroid – fietscomputer

Cycledroid neemt je ritten op met behulp van de GPS en toont de parameters van je rit, waaronder snelheid, afstand, tijd, hoogte, stijging, daling, hellinghoek, verbrande calorieën, vet en meer. Een rit kan worden weergegeven op een kaart en gepubliceerd worden naar Facebook. De applicatie staat toe om ritten aan te maken en opgenomen gegevens toe te kennen aan een specifieke rit. Er is een rittenoverzicht beschikbaar dat de totale afstand en tijd over alle ritten, etc. weergeeft.

Cycledroid kan alle gegevens exporteren naar een SD card in het CSV, GPX (GPS eXchange Format) of KML formaat; en CycleDroid kan gegevens in het CSV-formaat importeren. Dit maakt het mogelijk om gegevens in andere apps weer in te lezen of weer te geven op een kaart (Google Earth).

Cycledroid geeft de volgende grafieken weer: hoogte/afstand, snelheid/afstand en snelheid/tijd. Inzoomen op de grafiek gegevens is makkelijk en eenvoudig. Tevens is het mogelijk om een grafiek, of een deel ervan, op te slaan als een afbeelding.

Permissies:
– precieze locatie (GPS- en netwerkgebaseerd) – voor het vastleggen van beweging,
– de inhoud van uw USB-opslag aanpassen of verwijderen, testtoegang tot beveiligde opslag – voor het importeren en exporteren van rit gegevens,
– voorkomen dat apparaat overschakelt naar slaapmodus – zodat de gegevens ook worden opgenomen als het apparaat niet wordt gebruikt,
– volledige netwerktoegang, netwerkverbindingen weergeven, Google-serviceconfiguratie lezen – voor Google Maps en Facebook.

Het wordt op prijs gesteld als u de applicatie een waardering geeft in Play Store en fouten in de applicatie/vertaling meldt. Reacties en opmerkingen kunnen direct naar mij gestuurd worden of via de CycleDroid Facebook pagina: http://facebook.com/cycledroid. En als je wilt meehelpen met het vertalen van CycleDroid, ga naar deze pagina: http://crowdin.net/project/cycledroid.

===================================================

CycleDroid (doneren)

Deze versie van de applicatie wordt niet meer bijgehouden. Er is een gratis variant beschikbaar in de Play Store, die is identiek aan deze betaalde versie – graag deze downloaden. Als je mijn werk financieel wilt steunen, middels een donatie, dan kan dat ook vanuit de gratis app, middels een in-app betaalmogelijkheid.

===================================================

Doneren CycleDroid

Als je deze applicatie apprecieert, dan kun je geld doneren.

===================================================


– Turkse versie,
– Spaanse versie – met dank aan Francisco M. Román voor de vertaling,
– Bosnische versie – met dank aan Ishak Vehab voor de vertaling,
– smoother graphs,
– bug fixes,
– kleine wijzigingen in vertalingen.
