CycleDroid – licznik rowerowy

CycleDroid pozwala na śledzenie ruchu przy użycia GPS-a i wyświetlanie parametrów takich jak: prędkość, dystans, czas, wysokość, wzniesienia, spadki, nachylenie, spalone kalorie, tłuszcz i wiele więcej. Trasa może zostać wyświetlona na mapie oraz opublikowana na Facebooku. Aplikacja pozwala na tworzenie wycieczek i przypisywanie zebranych danych do wybranej wycieczki. Można również zobaczyć podsumowanie wybranych wycieczek: całkowity dystans, czas itd.

Wszystkie dane zebrane przez aplikację mogą zostać wyeksportowane na kartę pamięci w formacie CSV, a następnie zaimportowane do innego urządzenia z aplikacją CycleDroid. Można też zapisać dane do pliku GPX (GPS eXchange Format) lub KML i obejrzeć trasę na mapie na komputerze (na przykład w Google Earth).

CycleDroid posiada również możliwość rysowania wykresów: wysokość/dystans, prędkość/dystans, prędkość/czas. Można łatwo przybliżać i oddalać wykresy przy użyciu multi-touch. Aplikacja pozwala na zapisanie wybranego wykresu lub jego części jako plik graficzny.

Uprawnienia:
– dokładna lokalizacja (na podstawie sygnału GPS i sieci) – do śledzenia ruchu,
– modyfikowanie i usuwanie zawartości pamięci USB, testowanie dostępu do chronionej pamięci – do importowania i eksportowania wycieczek,
– zapobieganie przechodzeniu urządzenia w tryb uśpienia – aby móc pozostawić włączone śledzenie, gdy telefon nie jest używany,
– dostęp do internetu, wyświetlanie połączeń sieciowych, odczytywanie konfiguracji usług Google – dla Google Maps i Facebooka.

Zapraszam do oceniania, zgłaszania błędów w aplikacji lub tłumaczeniach, pozostawiania swoich opinii w komentarzach oraz kontaktowania się ze mną bezpośrednio lub przez profil CycleDroida na Facebooku: http://www.facebook.com/cycledroid. Jeśli spodobała ci się ta aplikacja, oceń ją na tej stronie. Jeśli chciałbyś mi pomóc i wziąć udział w tłumaczeniu CycleDroida, wejdź na tę stronę: http://crowdin.net/project/cycledroid.

===================================================

CycleDroid (wesprzyj)

Ta wersja aplikacji nie będzie już aktualizowana. W Sklepie Play jest dostępna bezpłatna wersja, która jest identyczna – możesz ją pobrać zamiast płatnej. Jeżeli chcesz wesprzeć moją pracę, możesz nadal to zrobić przy użyciu zakupów w aplikacji w wersji bezpłatnej.

===================================================

Wesprzyj aplikację CycleDroid

Jeżeli podoba ci się ta aplikacja, możesz wesprzeć moją pracę.

===================================================


– wersja turecka,
– wersja hiszpańska – podziękowania dla Francisco M. Román za tłumaczenie,
– wersja bośniacka – podziękowania dla Ishaka Vehaba za tłumaczenie,
– gładsze wykresy,
– poprawki błędów,
– drobne poprawki w tłumaczeniach.
